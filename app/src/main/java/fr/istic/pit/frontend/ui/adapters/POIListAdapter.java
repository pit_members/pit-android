package fr.istic.pit.frontend.ui.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import fr.istic.pit.frontend.R;
import fr.istic.pit.frontend.POIUtils;

/**
 * Adapter lié à un RecyclerView pour afficher la liste des pointOfInterest
 */
public class POIListAdapter extends RecyclerView.Adapter<POIListAdapter.IconViewHolder> {

    private List<String> icons = new ArrayList<>();
    private OnItemClickListener mListener;
    private Context context;
    private int selectedItem = RecyclerView.NO_POSITION;

    public interface OnItemClickListener{
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener mListener){
        this.mListener=mListener;
    }

    public POIListAdapter(Context context){
        this.context=context;
    }

    public static class IconViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        ImageView icon;
        public ConstraintLayout layout;

        IconViewHolder(View v, final OnItemClickListener listener) {
            super(v);
            name = v.findViewById(R.id.name);
            icon = v.findViewById(R.id.iconImage);
            layout = v.findViewById(R.id.gridLayoutItem);
            v.setOnClickListener(v1 -> {
                if(listener != null){
                    int position = getAdapterPosition();
                    if(position!=RecyclerView.NO_POSITION){
                        listener.onItemClick(position);
                    }
                }
            });
        }
    }


    public void setIcons(List<String> icons){
        this.icons = icons;
    }

    /**
     * Sélectionne l'item de la recyclerView à afficher comme sélectionné
     * @param position la position de l'item à sélectionner dans la liste des icones
     */
    public void setSelectedItem(int position){
        int old_pos = selectedItem;
        selectedItem = position;
        notifyItemChanged(old_pos);
        notifyItemChanged(selectedItem);
    }

    /**
     * @return le nom de l'icone actuellement sélectionnée
     */
    public String getSelectedItem(){
        return icons.get(selectedItem);
    }


    @NonNull
    @Override
    public IconViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.icon_item, parent, false);
        return new IconViewHolder(itemView, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull IconViewHolder holder, int position) {
        //TODO
        String s = icons.get(position);
        holder.name.setText(POIUtils.getDescription(s));
        holder.icon.setImageDrawable(context.getDrawable(POIUtils.getDrawable(s)));
        holder.itemView.setBackgroundColor(selectedItem == position ? Color.parseColor("#909090") : Color.TRANSPARENT);
    }

    @Override
    public int getItemCount() {
        return icons.size();
    }

}
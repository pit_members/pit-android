package fr.istic.pit.frontend.ui.fragments;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONException;
import org.json.JSONObject;
import org.osmdroid.config.Configuration;
import org.osmdroid.events.MapEventsReceiver;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.FolderOverlay;
import org.osmdroid.views.overlay.MapEventsOverlay;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.Overlay;
import org.osmdroid.views.overlay.Polyline;
import org.osmdroid.views.overlay.compass.CompassOverlay;
import org.osmdroid.views.overlay.compass.InternalCompassOrientationProvider;
import org.osmdroid.views.overlay.infowindow.InfoWindow;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fr.istic.pit.frontend.MarkerUtils;
import fr.istic.pit.frontend.R;
import fr.istic.pit.frontend.Utils;
import fr.istic.pit.frontend.model.Drone;
import fr.istic.pit.frontend.model.DroneMission;
import fr.istic.pit.frontend.model.DronePoint;
import fr.istic.pit.frontend.model.Intervention;
import fr.istic.pit.frontend.model.Mapper;
import fr.istic.pit.frontend.model.PointOfInterest;
import fr.istic.pit.frontend.model.Resource;
import fr.istic.pit.frontend.service.DroneService;
import fr.istic.pit.frontend.service.InterventionService;
import fr.istic.pit.frontend.service.PointOfInterestService;
import fr.istic.pit.frontend.service.RessourceService;
import fr.istic.pit.frontend.service.sync.SynchroClientService;
import fr.istic.pit.frontend.service.sync.UpdateEventType;
import fr.istic.pit.frontend.ui.activities.POIListActivity;
import fr.istic.pit.frontend.ui.activities.RequestResourcesActivity;
import fr.istic.pit.frontend.ui.activities.ResourceCRMActivity;
import fr.istic.pit.frontend.ui.markers.DronePointMarker;
import fr.istic.pit.frontend.ui.markers.MyRadiusMarkerClusterer;

import static android.app.Activity.RESULT_OK;
import static fr.istic.pit.frontend.service.sync.UpdateEventType.POI_CREATE;
import static fr.istic.pit.frontend.service.sync.UpdateEventType.POI_DISABLE;
import static fr.istic.pit.frontend.service.sync.UpdateEventType.POI_UPDATE;
import static fr.istic.pit.frontend.service.sync.UpdateEventType.RESOURCE_ARRIVE;
import static fr.istic.pit.frontend.service.sync.UpdateEventType.RESOURCE_MOVE;
import static fr.istic.pit.frontend.service.sync.UpdateEventType.RESOURCE_RELEASE;
import static fr.istic.pit.frontend.service.sync.UpdateEventType.RESOURCE_REQUEST_ACCEPT;
import static fr.istic.pit.frontend.service.sync.UpdateEventType.RESOURCE_REQUEST_CREATE;
import static fr.istic.pit.frontend.service.sync.UpdateEventType.RESOURCE_REQUEST_REJECT;
import static fr.istic.pit.frontend.service.sync.UpdateEventType.RESOURCE_UPDATE;


/**
 * Fragment pour la vue SITAC
 */
public class SitacViewFragment extends Fragment implements MapEventsReceiver {

    @BindView(R.id.carte)
    MapView map;

    @BindView(R.id.map_center)
    View mapCenter;

    @BindView(R.id.buttonDisplayCrm)
    Button displayCrmButton;

    @BindView(R.id.displayDrone)
    FloatingActionButton displayDrone;

    @BindView(R.id.droneButtonDisplayVideo)
    Button droneButtonDisplayVideo;

    @BindView(R.id.sitacDroneVideoPlaceholder)
    FrameLayout droneVideoPlaceholder;

    @BindView(R.id.crmLayout)
    ConstraintLayout crmLayout;

    @BindView(R.id.crmLayoutHideView)
    TextView hideTextCrmView;

    @BindView(R.id.lockUnlockEditFAB)
    FloatingActionButton lockUnlockEdit;

    @BindView(R.id.newIconFAB)
    FloatingActionButton selectIconToAdd;

    @BindView(R.id.newResourceFAB)
    FloatingActionButton selectResourceToAdd;

    @BindView(R.id.vsavDisponibleInt)
    TextView viewVsavDisponible;

    @BindView(R.id.vsavEnAttenteInt)
    TextView viewVsavDemande;

    @BindView(R.id.vsavEnRouteInt)
    TextView viewVsavEnRoute;

    @BindView(R.id.fptDisponibleInt)
    TextView viewFptDisponible;

    @BindView(R.id.fptEnAttenteInt)
    TextView viewFptDemande;

    @BindView(R.id.fptEnRouteInt)
    TextView viewFptEnRoute;

    @BindView(R.id.vlcgDisponibleInt)
    TextView viewVlcgDisponible;

    @BindView(R.id.vlcgDisponibleIcon)
    ImageView vlcgDisponibleIcon;

    @BindView(R.id.vlcgEnAttenteInt)
    TextView viewVlcgDemande;

    @BindView(R.id.vlcgEnAttenteIcon)
    ImageView vlcgEnAttenteIcon;

    @BindView(R.id.vlcgEnRouteInt)
    TextView viewVlcgEnRoute;

    @BindView(R.id.vlcgEnRouteIcon)
    ImageView vlcgEnRouteIcon;

    public MyRadiusMarkerClusterer folderResources;
    public MyRadiusMarkerClusterer folderPOI;
    private FolderOverlay folderDrone;
    private Marker droneMarker;

    // Le nombre de véhicules [TYPE][ETAT]
    private Map<String, Map<String, Integer>> resourceCounts;

    private boolean editEnabled = false;
    private boolean droneEnabled = false;
    private MarkerUtils markerUtils;
    private Context mContext;

    private Intervention intervention = new Intervention();

    private DroneFragment droneFragment;
    private DroneVideoFragment droneVideoFragment;

    private InterventionService interventionService;
    private RessourceService ressourceService;
    private PointOfInterestService pointOfInterestService;
    private DroneService droneService;
    private SynchroClientService synchroClient;

    private final int CODE_RETOUR_CREATION_SIG = 0;
    private final int CODE_RETOUR_PLACEMENT_RESOURCE = 1;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_sitac_view, container, false);
        ButterKnife.bind(this, v);

        markerUtils = new MarkerUtils(this, mContext);
        lockUnlockEdit.setBackgroundTintList(ColorStateList.valueOf(Color.RED));
        selectIconToAdd.setVisibility(View.INVISIBLE);
        selectResourceToAdd.setVisibility(View.INVISIBLE);
        mapCenter.setVisibility(View.INVISIBLE);

        // chargement de la carte
        loadMap();

        synchroClient = SynchroClientService.getService(getContext());
        interventionService = InterventionService.getService(getContext());
        ressourceService = RessourceService.getService(getContext());
        pointOfInterestService = PointOfInterestService.getService(getContext());
        droneService = DroneService.getService(getContext());

        // récuperation de l'ID de l'intervention
        if (getArguments() != null) {
            intervention.id = getArguments().getLong("interventionId", 1);
        } else {
            // Ne doit pas arriver
            intervention.id = 1L;
        }
        getIntervention(intervention.id);
        markerUtils.setMarkersDraggable(false);
        syncInit();
        displayVlcgIconsCRM();

        displayDrone.setBackgroundTintList(ColorStateList.valueOf(Color.RED));

        return v;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public Context getContext() {
        return this.mContext;
    }

    /**
     * Affiche l'encart du CRM
     */
    @OnClick(R.id.buttonDisplayCrm)
    void displayCRM() {
        displayCrmButton.setVisibility(View.GONE);
        crmLayout.setVisibility(View.VISIBLE);
    }

    /**
     * Cache l'encart du CRM
     */
    @OnClick(R.id.crmLayoutHideView)
    void hideTextCrmView() {
        displayCrmButton.setVisibility(View.VISIBLE);
        crmLayout.setVisibility(View.GONE);
    }

    /**
     * Active ou désactive l'édition
     * Lorsqu'on active l'édition, de nouvelles fonctionnalités sont disponibles, telles que l'ajout de POI ou Resource sur la carte
     * Cela permet également d'éditer les éléments présents sur la carte
     */
    @OnClick(R.id.lockUnlockEditFAB)
    void lockUnlockEdit() {
        if (editEnabled) {
            lockUnlockEdit.setBackgroundTintList(ColorStateList.valueOf(Color.RED));
            lockUnlockEdit.setImageResource(R.drawable.ic_verouille);
            selectIconToAdd.setVisibility(View.INVISIBLE);
            selectResourceToAdd.setVisibility(View.INVISIBLE);
            mapCenter.setVisibility(View.INVISIBLE);
            markerUtils.setMarkersDraggable(false);
        } else {
            lockUnlockEdit.setBackgroundTintList(ColorStateList.valueOf(Color.GREEN));
            lockUnlockEdit.setImageResource(R.drawable.ic_deverouille);
            selectIconToAdd.setVisibility(View.VISIBLE);
            selectResourceToAdd.setVisibility(View.VISIBLE);
            mapCenter.setVisibility(View.VISIBLE);
            markerUtils.setMarkersDraggable(true);
        }
        editEnabled = !editEnabled;
    }

    /**
     * Lance l'activité {@link POIListActivity}
     */
    @OnClick(R.id.newIconFAB)
    void selectIconToAdd() {
        Intent intent = new Intent(getActivity(), POIListActivity.class);
        startActivityForResult(intent, CODE_RETOUR_CREATION_SIG);
    }

    /**
     * Lance l'activité {@link ResourceCRMActivity}
     * Sélectionne les resources de l'intervention à passer via l'intent
     * Les resources sélectionnées sont celle qui ne sont pas sur la carte
     */
    @OnClick(R.id.newResourceFAB)
    void selectResourceToAdd() {
        Intent intent = new Intent(getActivity(), ResourceCRMActivity.class);

        ArrayList<Resource> resources = new ArrayList<>();
        for(Resource res : intervention.resources){
            if(
                res.crm &&
                !res.macroState.equals("END") &&
                !res.macroState.equals("CANCELLED") &&
                !res.macroState.equals("RELEASED") &&
                !res.macroState.equals("REJECTED") &&
                (res.latitude==null || res.latitude==0L) &&
                (res.longitude==null||res.longitude==0L)
            ){
                resources.add(res);
            }
        }
        intent.putParcelableArrayListExtra("resources", resources);
        startActivityForResult(intent, CODE_RETOUR_PLACEMENT_RESOURCE);
    }

    /**
     * Lance l'activité {@link RequestResourcesActivity} pour demander des moyens
     */
    @OnClick(R.id.newResourceRequestButton)
    void requestResource() {
        Intent nextActivity = new Intent(getActivity(), RequestResourcesActivity.class);
        nextActivity.putExtra("interventionId", intervention.id);
        nextActivity.putExtra("sinisterCode", intervention.sinisterCode);
        startActivity(nextActivity);
    }

    /**
     * Affiche ou cache les éléments liés au drone
     * Le bouton n'est visible que si un drone est rattaché à l'intervention
     */
    @OnClick(R.id.displayDrone)
    void onClickDisplayDrone() {
        this.droneEnabled = !droneEnabled;
        if(!droneEnabled) { // Etait visible précédemment
            this.map.getOverlays().remove(folderDrone);
            displayDrone.setBackgroundTintList(ColorStateList.valueOf(Color.RED));
            droneButtonDisplayVideo.setVisibility(View.GONE);
            droneVideoPlaceholder.setVisibility(View.GONE);
        } else {
            this.map.getOverlays().add(folderDrone);
            displayDrone.setBackgroundTintList(ColorStateList.valueOf(Color.GREEN));
            droneButtonDisplayVideo.setVisibility(View.VISIBLE);
            droneButtonDisplayVideo.setText("\\\n/");
        }
        this.map.invalidate();
    }

    /**
     * Affiche ou cache l'encart vidéo du drone
     */
    @OnClick(R.id.droneButtonDisplayVideo)
    void onClickDisplayVideo(){
        if(droneVideoPlaceholder.getVisibility()==View.VISIBLE){
            droneVideoPlaceholder.setVisibility(View.GONE);
            droneButtonDisplayVideo.setText("\\\n/");
            droneVideoFragment.setShouldDisplay(false);
        }else {
            droneVideoPlaceholder.setVisibility(View.VISIBLE);
            droneButtonDisplayVideo.setText("/\n\\");
            droneVideoFragment.setShouldDisplay(true);
        }
    }

    /**
     * Récupération de l'intervention à afficher en se basant sur son ID
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void getIntervention(Long interventionId) {

        interventionService.getIntervention(
                interventionId,
                response -> {
                    try {
                        this.intervention = Mapper.parseIntervention(response);
                        // update du centre de la carte
                        map.getController().setCenter(new GeoPoint(this.intervention.latitude, this.intervention.longitude));
                    } catch (JSONException e) {
                        Toast.makeText(getContext(), "Erreur de format de l'intervention obtenue.", Toast.LENGTH_LONG).show();
                    }
                },
                error ->Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_LONG).show()
        );

        getPOIsFromBackAndDisplay(interventionId);
        getResourcesFromBackAndDisplay(interventionId);
    }

    /**
     * Récupère la liste des {@link PointOfInterest} de l'intervention
     * @param interventionId l'intervention dont on veut récupérer les points
     */
    private void getPOIsFromBackAndDisplay(Long interventionId) {
        pointOfInterestService.getPOIList(
                interventionId,
                response -> {
                    try {
                        this.intervention.pointsOfInterest = Mapper.parsePointOfInterest(response);
                        loadOverlayPOI();
                    } catch (JSONException e) {
                        Toast.makeText(getContext(), "Erreur de format des points d'intérêt obtenus.", Toast.LENGTH_LONG).show();
                    }
                },
                error -> Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_LONG).show()
        );
    }

    /**
     * Récupère la liste des {@link Resource} de l'intervention
     * @param interventionId l'intervention dont on veut récupérer les resources
     */
    private void getResourcesFromBackAndDisplay(Long interventionId) {
        ressourceService.getRessourceListByInterventionId(
                interventionId,
                response -> {
                    try {
                        this.intervention.resources = Mapper.parseResource(response);
                        loadOverlayResource();
                        loadCRM();
                    } catch (JSONException e) {
                        Toast.makeText(getContext(), "Erreur de format des ressources obtenues.", Toast.LENGTH_LONG).show();
                    }
                },
                error -> Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_LONG).show()
        );
    }

    /**
     * Initie la synchronisation et spécifie les actions à effectuer à la réception d'un message
     */
    private void syncInit() {

        // Evènements globaux
        synchroClient.subscribeTo(event -> {
            UpdateEventType type = event.getType();
            if (type.equals(UpdateEventType.INTERV_DISABLE)) {
                // Fin d'intervention
            }
        });

        // Evènements spécifiques à l'interventions
        synchroClient.subscribeTo(intervention.id, event -> {
            UpdateEventType type = event.getType();
            if (type == RESOURCE_RELEASE ||
                type == RESOURCE_MOVE ||
                type == RESOURCE_ARRIVE ||
                type == RESOURCE_UPDATE ||
                type == RESOURCE_REQUEST_CREATE ||
                type == RESOURCE_REQUEST_ACCEPT ||
                type == RESOURCE_REQUEST_REJECT) {
                updateResources();
            } else if (type == POI_CREATE ||
                       type == POI_DISABLE ||
                       type == POI_UPDATE) {
                updatePOIs();
            }
        });

    }

    /**
     * Définit la couleur des VLCG dans l'encart du CRM en fonction du code sinistre de l'intervention
     */
    private void displayVlcgIconsCRM(){
        int vlcgDispoIcon = R.drawable.ic_vehicule_actif_feu;
        int vlcgOtherIcon = R.drawable.ic_vehicule_prevu_feu;

        if (getArguments() != null){
            String sinisterCode = getArguments().getString("sinisterCode");
            if (sinisterCode != null && sinisterCode.equals(Utils.ALP)) {
                vlcgDispoIcon = R.drawable.ic_vehicule_actif_humain;
                vlcgOtherIcon = R.drawable.ic_vehicule_prevu_humain;
            }
        }
        vlcgDisponibleIcon.setImageDrawable(mContext.getDrawable(vlcgDispoIcon));
        vlcgEnRouteIcon.setImageDrawable(mContext.getDrawable(vlcgOtherIcon));
        vlcgEnAttenteIcon.setImageDrawable(mContext.getDrawable(vlcgOtherIcon));
    }

    /**
     * Vide la liste des markers représentant les pointOfInterest pour le reremplir avec la version actuelle en base de donnée
     */
    private void updatePOIs() {
        folderPOI.getItems().clear();
        getPOIsFromBackAndDisplay(intervention.id);
    }

    /**
     * Vide la liste des markers représentant les Resources pour le reremplir avec la version actuelle en base de donnée
     */
    private void updateResources() {
        folderResources.getItems().clear();
        getResourcesFromBackAndDisplay(intervention.id);
    }

    /**
     * Chargement de la carte et initialisation des folders d'Overlays
     */
    private void loadMap() {
        // permission de localisation et de lecture de mémoire pour récup la carte
        ActivityCompat.requestPermissions(Objects.requireNonNull(getActivity()),
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        // chargement carte
        Configuration.getInstance().load(getActivity().getApplicationContext(), PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext()));
        // positionnement sur la carte (zoom + emplacement) et affichage orientation via boussole
        map.getController().setZoom(15.0);
        // map.getZoomController().setVisibility(CustomZoomButtonsController.Visibility.NEVER);
        CompassOverlay compassOverlay = new CompassOverlay(getActivity().getApplicationContext(), new InternalCompassOrientationProvider(getActivity().getApplicationContext()), map);
        compassOverlay.enableCompass();
        map.getOverlays().add(compassOverlay);
        map.setMultiTouchControls(true);
        map.setDestroyMode(false);

        folderResources = new MyRadiusMarkerClusterer(mContext, mContext.getDrawable(R.drawable.seul_cluster));
        folderPOI = new MyRadiusMarkerClusterer(mContext, mContext.getDrawable(R.drawable.poi_cluster));
        folderDrone = new FolderOverlay();

        map.getOverlays().add(folderResources);
        map.getOverlays().add(folderPOI);

        MapEventsOverlay mapEventsOverlay = new MapEventsOverlay(this);
        map.getOverlays().add(0, mapEventsOverlay);
    }

    /**
     * Mets à jour la position d'un poi dans le back après validation de la modification
     *
     * @param poi Le point d'intérêt
     */
    public void updatePOILocation(final PointOfInterest poi) {
        try {
            pointOfInterestService.movePOI(
                    intervention.id,
                    poi,
                    response -> {},
                    Throwable::printStackTrace
            );
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Ajoute un nouveau poi dans le back
     * @param poi le poi à ajouter
     */
    private void createPOI(PointOfInterest poi) {
        try {
            pointOfInterestService.createPOI(
                    intervention.id,
                    poi,
                    response -> {
                    },
                    Throwable::printStackTrace
            );
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Supprime un poi dans le back
     * @param poi le poi à supprimer
     */
    public void deletePOI(PointOfInterest poi) {
        pointOfInterestService.deletePOI(
                intervention.id,
                poi.id,
                response -> {
                },
                Throwable::printStackTrace
        );
    }

    /**
     * Met à jour le role d'une resource dans le back
     * @param resource la resource à mettre à jour
     */
    public void updateRoleResource(Resource resource){
        ressourceService.roleRessource(
                intervention.id,
                resource.id,
                resource.role,
                response -> {},
                Throwable::printStackTrace
        );
    }

    /**
     * Met à jour la position d'une resource dans le back
     * @param resource la resource à déplacer
     */
    public void updateResourceLocation(Resource resource){
        ressourceService.moveRessource(
                intervention.id,
                resource.id,
                resource.latitude,
                resource.longitude,
                response -> {},
                Throwable::printStackTrace
        );
    }

    /**
     * Retourne une reousrce dans le crm (dans le back)
     * @param resource la resource à retourner dans le crm
     */
    public void moveResourceToCrm(Resource resource){
        ressourceService.crmRessource(
                intervention.id,
                resource.id,
                response -> {},
                Throwable::printStackTrace
        );
    }

    /**
     * Notifie au back l'arrivée d'une ressource sur l'intervention
     * @param resource la resource concernée
     */
    public void validateResource(Resource resource){
        ressourceService.arriveRessource(
                intervention.id,
                resource.id,
                response -> {},
                Throwable::printStackTrace
        );
    }

    /**
     * Calcule le nombre de véhicules dans chaque état présent dans le crm en se basant sur les resources associées à l'intervention
     */
    private void loadCRM() {
        if (intervention != null) {
            // Initialisation du tableau contenant le nombre de véhicules par type et état
            resourceCounts = new HashMap<>();

            for (Resource resource : intervention.resources) {
                if (resource.crm) {
                    resourceCounts.putIfAbsent(resource.type, new HashMap<>());
                    resourceCounts.get(resource.type).putIfAbsent(resource.macroState, 0);
                    resourceCounts.get(resource.type).computeIfPresent(resource.macroState, (k, v) -> v + 1);
                }
            }
        }

        viewVsavDisponible.setText(String.valueOf(getResourceCount("VSAV", "ARRIVED")));
        viewVsavEnRoute.setText(String.valueOf(getResourceCount("VSAV", "COMING")));
        viewVsavDemande.setText(String.valueOf(getResourceCount("VSAV", "WAITING")));
        viewFptDisponible.setText(String.valueOf(getResourceCount("FPT", "ARRIVED")));
        viewFptEnRoute.setText(String.valueOf(getResourceCount("FPT", "COMING")));
        viewFptDemande.setText(String.valueOf(getResourceCount("FPT", "WAITING")));
        viewVlcgDisponible.setText(String.valueOf(getResourceCount("VLCG", "ARRIVED")));
        viewVlcgEnRoute.setText(String.valueOf(getResourceCount("VLCG", "COMING")));
        viewVlcgDemande.setText(String.valueOf(getResourceCount("VLCG", "WAITING")));
    }

    /**
     * Compte les resources d'une intervention avec des critères de type et état
     * @param type le type des resources à compter
     * @param state l'état des resources à compter
     * @return la somme des resources dont le type et l'état sont identiques à ceux en paramètres
     */
    private int getResourceCount(String type, String state) {
        if (resourceCounts == null)
            return 0;

        if (!resourceCounts.containsKey(type))
            return 0;

        return resourceCounts.get(type).getOrDefault(state, 0);
    }

    /**
     * Place sur la carte les POIs
     **/
    private void loadOverlayPOI() {
        // affichage des sigs spécifiques à l'intervention
        if (null != intervention && null != intervention.pointsOfInterest && intervention.pointsOfInterest.size() > 0) {
            for (PointOfInterest poi : intervention.pointsOfInterest) {
                try {
                    Marker marker = markerUtils.createMarker(map, poi);
                    marker.setDraggable(editEnabled);
                } catch (NullPointerException  e) {
                    Toast.makeText(mContext,"Une erreur de création de marker s'est produite",Toast.LENGTH_SHORT).show();
                }
            }
        }
        map.invalidate();
    }

    /**
     * Place sur la carte les Resources/moyens
     **/
    private void loadOverlayResource() {
        // affichage des sigs spécifiques à l'intervention
        if (null != intervention && null != intervention.resources && intervention.resources.size() > 0) {
            for (Resource res : intervention.resources) {
                if (!res.crm) {
                    Marker marker = markerUtils.createMarkerwithLabel(map, res);
                    marker.setDraggable(editEnabled);
                }
            }
        }
        map.invalidate();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser && null!=droneFragment && null!=droneFragment.getMapDrone()) {
            MapView mapDrone = droneFragment.getMapDrone();
            this.map.getController().setCenter(mapDrone.getMapCenter());
            this.map.getController().setZoom(mapDrone.getZoomLevelDouble());
        }
        if(isVisibleToUser && null!=droneVideoFragment){
            droneVideoFragment.setShouldDisplay(droneVideoPlaceholder.getVisibility() == View.VISIBLE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CODE_RETOUR_CREATION_SIG) {
            if (resultCode == RESULT_OK) {
                // Un item a été sélectionné
                String icon = data.getStringExtra("IdIcon");
                PointOfInterest poi = new PointOfInterest(new GeoPoint(map.getMapCenter()), icon);
                createPOI(poi);
            }
        } else if (requestCode == CODE_RETOUR_PLACEMENT_RESOURCE) {
            if (resultCode == RESULT_OK) {
                //Un item a été sélectionné
                long id = data.getLongExtra("IdResource", 0L);
                for (Resource resource : intervention.resources){
                    if (id == resource.id){
                        GeoPoint point = new GeoPoint(map.getMapCenter());
                        resource.longitude = point.getLongitude();
                        resource.latitude = point.getLatitude();
                        updateResourceLocation(resource);
                        break;
                    }
                }
            }
        }
    }

    /**
     * Ferme toutes les infoWindow des markers sur la carte
     * @param p le point qui a été cliqué
     * @return true
     */
    @Override
    public boolean singleTapConfirmedHelper(GeoPoint p) {
        InfoWindow.closeAllInfoWindowsOn(map);
        return true;
    }

    @Override
    public boolean longPressHelper(GeoPoint p) {
        return false;
    }

    public boolean isEditEnabled() {
        return editEnabled;
    }

    /**
     * Ajoute le fragment du drone dans le placeholder prévu à cet effet et initialise la synchronisation pour ce fragment
     * @param droneFragment
     */
    void setDroneFragment(DroneFragment droneFragment){
        this.droneFragment = droneFragment;
        this.displayDrone.setVisibility(View.VISIBLE);

        droneVideoFragment = new DroneVideoFragment(intervention.id);
        getChildFragmentManager().beginTransaction().add(R.id.sitacDroneVideoPlaceholder, droneVideoFragment, "VideoTag").commit();

        getMission();
        synchroClient.subscribeTo(intervention.id, event -> {
            switch (event.getType()) {
                case DRONE_STATE:
                    try {
                        JSONObject data = event.getData();
                        updateDronePosition(
                                data.getDouble("latitude"),
                                data.getDouble("longitude"),
                                data.getDouble("altitude"),
                                data.getString("state"));
                    } catch (JSONException e) {
                        e.printStackTrace(); // Should never happen
                    }
                    break;
                case MISSION_CREATE:
                    getMission();
                    break;
                default:
                    break;
            }
        });
    }

    /**
     * @return la carte de la SITAC
     */
    public MapView getMap() {
        return map;
    }

    /**
     * Mets à jour la position du drone sur la carte et son état
     * @param latitude la nouvelle latitude du drone
     * @param longitude la nouvelle longitude du drone
     * @param altitude la nouvelle altitude du drone
     * @param state le nouvel état du drone
     */
    private void updateDronePosition(double latitude, double longitude, double altitude, String state) {
        if(droneMarker == null) {
            droneMarker = new Marker(map);
            droneMarker.setPosition(new GeoPoint(latitude, longitude));
            droneMarker.setAnchor((float) 0.5, (float) 0.5);
            droneMarker.setInfoWindow(null);
            folderDrone.getItems().add(droneMarker);
        } else {
            droneMarker.setPosition(new GeoPoint(latitude, longitude));
        }
        updateDroneIcon(Drone.State.valueOf(state));

        if(droneEnabled)
            map.invalidate();
    }

    /**
     * Mets à jour l'icone représentant le drone sur la carte en fonction de son état
     * @param state l'état du drone
     */
    private void updateDroneIcon(Drone.State state) {
        switch(state){
            case IDLE:droneMarker.setIcon(mContext.getDrawable(R.drawable.drone_aerostationnement));break;
            case GROUND:droneMarker.setIcon(mContext.getDrawable(R.drawable.drone_au_sol));break;
            case MOVING:droneMarker.setIcon(mContext.getDrawable(R.drawable.drone_deplacement));break;
            default:break;
        }
    }

    /**
     * Récupère dans le back la mission du drone lié à cette intervention
     */
    private void getMission() {
        List<Overlay> onlyDroneMarker = folderDrone.getItems().stream().filter(overlay -> overlay == droneMarker).collect(Collectors.toList());
        folderDrone.getItems().clear();
        folderDrone.getItems().addAll(onlyDroneMarker);

        droneService.getMission(intervention.id,
                response -> {
                    DroneMission mission = new DroneMission();
                    try {
                        mission = Mapper.parseDroneMission(response);
                    } catch (JSONException e) {
                        // mission is null > empty mission
                    }
                    for(DronePoint point : mission.points) {
                        DronePointMarker m = new DronePointMarker(mContext, map, point);
                        m.setDraggable(false);
                        folderDrone.getItems().add(0,m);
                    }
                    Polyline route = new Polyline();
                    route.getOutlinePaint().setColor(Color.parseColor("#000000"));
                    route.getOutlinePaint().setStrokeWidth(2);
                    List<GeoPoint> routePoints = new ArrayList<>();
                    for(DronePoint point : mission.points) {
                        routePoints.add(new GeoPoint(point.latitude, point.longitude));
                    }
                    if (!routePoints.isEmpty() && mission.missionLoop)
                        routePoints.add(routePoints.get(0));
                    route.setPoints(routePoints);
                    folderDrone.getItems().add(0,route);
                    if(droneEnabled)
                        map.invalidate();
                },
                Throwable::printStackTrace);
    }
}

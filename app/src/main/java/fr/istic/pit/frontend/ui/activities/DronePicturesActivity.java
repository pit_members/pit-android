package fr.istic.pit.frontend.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.DisplayMetrics;

import org.json.JSONException;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fr.istic.pit.frontend.R;
import fr.istic.pit.frontend.model.Mapper;
import fr.istic.pit.frontend.service.DroneService;
import fr.istic.pit.frontend.ui.adapters.DronePicturesAdapter;

/**
 * Activity affichant les photos prises par un drône sur un point précis
 * L'id de l'intervention concernée, la latitude et longitude du point photographié sont à passer en extra à l'Intent
 */
public class DronePicturesActivity extends AppCompatActivity {

    @BindView(R.id.dronePicturesRecyclerView) RecyclerView recyclerView;
    private DronePicturesAdapter adapter;
    private Long interventionId;
    private double latitude;
    private double longitude;

    private DroneService droneService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drone_pictures);
        ButterKnife.bind(this);

        interventionId = getIntent().getLongExtra("interventionId",-1);
        latitude = getIntent().getDoubleExtra("latitude",0);
        longitude = getIntent().getDoubleExtra("longitude",0);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new DronePicturesAdapter(getApplicationContext(), interventionId, latitude, longitude);
        recyclerView.setAdapter(adapter);

        droneService = DroneService.getService(getApplicationContext());
        getPicturesNames();


        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
        int height = dm.heightPixels;
        getWindow().setLayout((int)(width*.8),(int)(height*.7));
    }

    /**
     * Quitte l'activity
     */
    @OnClick(R.id.dronePicturesQuitButton)
    public void quit(){
        finish();
    }

    /**
     * Récupère les noms des photos prises sur ce point via le service droneService
     */
    private void getPicturesNames(){
        droneService.getListPhotoName(
            interventionId,
            latitude,
            longitude,
            response -> {
                try {
                    List<String> pictures = Mapper.parsePicturesNames(response);
                    Collections.sort(pictures, Collections.reverseOrder());
                    adapter.setNames(pictures);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            },
            Throwable::printStackTrace);
    }
}

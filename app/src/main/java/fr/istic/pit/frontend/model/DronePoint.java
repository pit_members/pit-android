package fr.istic.pit.frontend.model;

/**
 * Classe DTO d'un point d'arrêt sur le parcours d'un drone
 */
public class DronePoint {

    /**
     * ID du dronePoint
     */
    public long id;

    /**
     * ID de la mission à laquelle est rattaché le point
     */
    public long missionId;

    /**
     * Ordre du point dans la liste {@link DroneMission#points}
     */
    public int orderNumber;

    /**
     * Latitude
     */
    public double latitude;

    /**
     * Longitude
     */
    public double longitude;

    /**
     * altitude
     */
    public double altitude = 20;

    public DronePoint(){}
}

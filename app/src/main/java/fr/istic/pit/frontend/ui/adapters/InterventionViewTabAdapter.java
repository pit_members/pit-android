package fr.istic.pit.frontend.ui.adapters;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class InterventionViewTabAdapter extends FragmentPagerAdapter {

    private List<Fragment> fragmentTabList = new ArrayList<>();
    private List<String> titleTabList = new ArrayList<>();

    public InterventionViewTabAdapter(@NonNull FragmentManager fm) {
        super(fm);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return fragmentTabList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentTabList.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return titleTabList.get(position);
    }

    public void addFragment(Fragment fragment, String title) {
        fragmentTabList.add(fragment);
        titleTabList.add(title);
    }
}

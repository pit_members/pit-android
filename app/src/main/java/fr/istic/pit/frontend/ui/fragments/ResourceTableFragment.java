package fr.istic.pit.frontend.ui.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONException;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import fr.istic.pit.frontend.R;
import fr.istic.pit.frontend.model.Mapper;
import fr.istic.pit.frontend.model.Resource;
import fr.istic.pit.frontend.service.RessourceService;
import fr.istic.pit.frontend.service.sync.SynchroClientService;
import fr.istic.pit.frontend.service.sync.UpdateEventType;
import fr.istic.pit.frontend.ui.activities.RequestResourcesActivity;
import fr.istic.pit.frontend.ui.adapters.ResourceTableAdapter;

/**
 * Fragment pour le tableau des moyens
 */
public class ResourceTableFragment extends Fragment {


    private static final String TAG = ResourceTableFragment.class.getSimpleName();

    private SynchroClientService synchroClient;

    @BindView(R.id.tabMoyensRecyclerView) RecyclerView recyclerView;
    private ResourceTableAdapter mAdapter;

    RessourceService ressourceService;
    Long interventionId;
    boolean isCodis;
    String sinisterCode;

    public ResourceTableFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_resource_table, container, false);
        ButterKnife.bind(this, view);

        if (getArguments() != null) {
            interventionId = getArguments().getLong("interventionId");
            isCodis = getArguments().getBoolean("isCodis");
            sinisterCode = getArguments().getString("sinisterCode");
        }else{
            interventionId = 1L;
            isCodis = false;
        }

        synchroClient = SynchroClientService.getService(getContext());

        mAdapter = new ResourceTableAdapter(getContext(),isCodis);
        recyclerView.setAdapter(mAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(dividerItemDecoration);

        update();

        syncInit();

        Button buttonRequestResource = view.findViewById(R.id.tabMoyens_buttonAdd);

        buttonRequestResource.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //update();

                Intent requestResources = new Intent(getActivity(), RequestResourcesActivity.class);
                requestResources.putExtra("interventionId", interventionId);
                requestResources.putExtra("sinisterCode", sinisterCode);
                startActivity(requestResources);
            }
        });

        return view;
    }

    /**
     * Synchronisation avec MQTT
     * Filtrage en fonction de l'intervention et des types liés au tableau des moyens
     */
    private void syncInit() {
        // Evènements spécifiques à l'interventions
        synchroClient.subscribeTo(interventionId, event -> {
            UpdateEventType type = event.getType();
            if(
                type.equals(UpdateEventType.RESOURCE_REQUEST_CREATE)||
                type.equals(UpdateEventType.RESOURCE_REQUEST_ACCEPT)||
                type.equals(UpdateEventType.RESOURCE_REQUEST_REJECT)||
                type.equals(UpdateEventType.RESOURCE_RELEASE)||
                type.equals(UpdateEventType.RESOURCE_MOVE)||
                type.equals(UpdateEventType.RESOURCE_ARRIVE)||
                type.equals(UpdateEventType.RESOURCE_UPDATE)
            )
                notifyUpdate(event.getType().toString());
        });

    }


    /**
     * Récupère les nouvelles ressources lors d'une notification MQTT de mise à jour
     */
    public void update() {
        ressourceService = RessourceService.getService(getContext());
        ressourceService.getRessourceListByInterventionId(
                interventionId,
                response -> {
                    try {
                        List<Resource> resources = Mapper.parseResource(response);
                        Collections.sort(resources);
                        mAdapter.clear();
                        for(Resource resource : resources)
                            mAdapter.add(resource);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },
                error -> Log.d(TAG, String.valueOf(error)));
    }


    /**
     * Gère l'affichage d'un Toast lorsqu'il y a une mise à jour
     * Le Toast est personnalisé en fonction du type
     * @param type Le type du message MQTTT
     */
    public void notifyUpdate(String type) {
        update();
        displayToast(type);
    }


    /**
     * Gère l'affichage d'un Toast personnalisé en transformant le type MQTT en texte
     * @param type Le type de message MQTT
     */
    public void displayToast(String type) {

        String text = "";

        switch (type) {
            case "RESOURCE_RELEASE":
                text = "Moyen libéré";
                customToast(text);
                break;
            case "RESOURCE_ARRIVE":
                text = "Moyen arrivé";
                customToast(text);
                break;
            case "RESOURCE_UPDATE":
                text = "Moyen mis à jour";
                customToast(text);
                break;
            case "RESOURCE_REQUEST_CREATE":
                text = "Moyen créé";
                customToast(text);
                break;
            case "RESOURCE_REQUEST_ACCEPT":
                text = "Moyen accepté";
                customToast(text);
                break;
            case "RESOURCE_REQUEST_REJECT":
                text = "Moyen rejeté/annulé";
                customToast(text);
                break;
            case "RESOURCE_MOVE":
                text = "Moyen en déplacement";
                customToast(text);
                break;
            default:
                text = "";
        }

    }


    /**
     * Créé un Toast personnalisé
     * @param text Le text à afficher dans le Toast
     */
    public void customToast(String text) {
        // custom Toast
        Toast toast = Toast.makeText(getActivity(),text,Toast.LENGTH_LONG);
        toast.setGravity(Gravity.TOP|Gravity.CENTER, 0, 10);
        View view = toast.getView();
        TextView  textView = view.findViewById(android.R.id.message);
        view.getBackground().setColorFilter(Color.DKGRAY, PorterDuff.Mode.SRC_IN);
        textView.setTextColor(Color.WHITE);
        toast.show();

    }



}







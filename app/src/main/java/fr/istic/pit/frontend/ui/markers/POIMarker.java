package fr.istic.pit.frontend.ui.markers;

import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;

import fr.istic.pit.frontend.model.PointOfInterest;

/**
 * Marker associé à un poi
 */
public class POIMarker extends Marker {

    private GeoPoint oldPosition;
    private PointOfInterest poi;

    public POIMarker(MapView mapView, PointOfInterest poi) {
        super(mapView);
        this.poi = poi;
    }

    public PointOfInterest getPoi(){
        return poi;
    }

    public GeoPoint getOldPosition() {
        return oldPosition;
    }

    public void setOldPosition(GeoPoint oldPosition) {
        this.oldPosition = oldPosition;
    }
}

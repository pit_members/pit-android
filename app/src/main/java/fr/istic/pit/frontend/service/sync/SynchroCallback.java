package fr.istic.pit.frontend.service.sync;

/**
 * L'interface définissant la méthode de rappel appelée par le service de synchro
 * lorsqu'un évènement de mise à jour est reçu.
 */
public interface SynchroCallback {
    /**
     * Notification de mise à jour reçue.
     * @param event L'évènement reçu
     */
    void onUpdateReceived(UpdateEvent event);
}

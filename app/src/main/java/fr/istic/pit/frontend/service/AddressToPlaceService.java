package fr.istic.pit.frontend.service;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;


public class AddressToPlaceService extends AbstractService {

    private final static String API_TOKEN = "5b3ce3597851110001cf62481014036d10f14794bd39fd12915a7e32";
    private final static String URL =
            "https://api.openrouteservice.org/geocode/search/structured?api_key=" + API_TOKEN + "&country=france";

    public static AddressToPlaceService getService(Context context) {
        return new AddressToPlaceService(context);
    }

    private AddressToPlaceService(Context context) {
        super(context);
    }

    public void getCoordinates(String address, String postalCode, String city, Response.Listener<JSONObject> onSuccess, Response.ErrorListener onError) {
        JsonObjectRequest request = new AbstractService.EmptyResponseJsonObjectRequest(
                Request.Method.GET,
                URL + "&address="+address + "&postalcode="+postalCode + "&locality="+city,
                null,
                onSuccess,
                onError);
        requestQueue.add(request);
    }

    public void getCoordinates(String address, String postalCode, Response.Listener<JSONObject> onSuccess, Response.ErrorListener onError) {
        JsonObjectRequest request = new AbstractService.EmptyResponseJsonObjectRequest(
                Request.Method.GET,
                URL + "&address="+address + "&postalcode="+postalCode,
                null,
                onSuccess,
                onError);
        requestQueue.add(request);
    }

    public void getCoordinates(String address, Response.Listener<JSONObject> onSuccess, Response.ErrorListener onError) {
        JsonObjectRequest request = new AbstractService.EmptyResponseJsonObjectRequest(
                Request.Method.GET,
                URL + "&address="+address,
                null,
                onSuccess,
                onError);
        requestQueue.add(request);
    }

}
package fr.istic.pit.frontend.ui.markers;

import android.widget.ImageView;

import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.infowindow.InfoWindow;

import fr.istic.pit.frontend.R;
import fr.istic.pit.frontend.ui.fragments.SitacViewFragment;

/**
 * Infowindow permettant de supprimer un poi
 */
public class DeleteInfoWindow extends InfoWindow {

    private POIMarker marker;
    private SitacViewFragment sitacViewFragment;

    public DeleteInfoWindow(MapView mapView, SitacViewFragment sitacViewFragment, POIMarker marker) {
        super(R.layout.infowindows_delete, mapView);
        this.marker = marker;
        this.sitacViewFragment = sitacViewFragment;
    }

    public void onClose() {
        // méthode non utile
    }

    public void onOpen(Object arg0) {
        final ImageView delete = mView.findViewById(R.id.deleteMarker);
        delete.setOnClickListener(InfoWindowUtils.getDeleteListener(sitacViewFragment, marker, this, sitacViewFragment.folderPOI));
    }
}

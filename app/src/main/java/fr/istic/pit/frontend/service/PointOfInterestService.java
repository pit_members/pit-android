package fr.istic.pit.frontend.service;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import fr.istic.pit.frontend.model.Mapper;
import fr.istic.pit.frontend.model.PointOfInterest;

/**
 * Service effectuant les requêtes au back pour les {@link PointOfInterest}
 */
public class PointOfInterestService extends AbstractService {

    public static PointOfInterestService getService(Context context) {
        return new PointOfInterestService(context);
    }

    protected PointOfInterestService(Context context) {
        super(context);
    }

    /**
     * Récupère la liste des poi d'une intervention
     * @param interventionId ID de l'intervention
     * @param onSuccess callback si succès
     * @param onError callback si erreur
     */
    public void getPOIList(Long interventionId, Response.Listener<JSONArray> onSuccess, Response.ErrorListener onError) {
        JsonArrayRequest request = new EmptyResponseJsonArrayRequest(
                Request.Method.GET,
                URL + "/interventions/" + interventionId + "/pointsOfInterest",
                null,
                onSuccess,
                onError);
        requestQueue.add(request);
    }

    /**
     * Crée un poi
     * @param interventionID ID de l'intervention
     * @param poi le poi
     * @param onSuccess callback si succès
     * @param onError callback si erreur
     * @throws JSONException
     */
    public void createPOI(Long interventionID, PointOfInterest poi, Response.Listener<JSONObject> onSuccess, Response.ErrorListener onError) throws JSONException {
        JsonObjectRequest request = new EmptyResponseJsonObjectRequest(
                Request.Method.POST,
                URL + "/interventions/"+interventionID+"/pointsOfInterest",
                Mapper.pointOfInterestToJSON(poi),
                onSuccess,
                onError);
        requestQueue.add(request);
    }

    /**
     * Supprime un poi
     * @param interventionId ID de l'intervention
     * @param poiId ID du poi à supprimer
     * @param onSuccess callback si succès
     * @param onError callback si erreur
     */
    public void deletePOI(Long interventionId, Long poiId, Response.Listener<JSONObject> onSuccess, Response.ErrorListener onError) {
        JsonObjectRequest request = new EmptyResponseJsonObjectRequest(
                Request.Method.DELETE,
                URL + "/interventions/" + interventionId + "/pointsOfInterest/" + poiId,
                null,
                onSuccess,
                onError);
        requestQueue.add(request);
    }

    /**
     * Déplace un poi
     * @param interventionID ID de l'intervention
     * @param poi le poi à déplacer
     * @param onSuccess callback si succès
     * @param onError callback si erreur
     * @throws JSONException
     */
    public void movePOI(Long interventionID, PointOfInterest poi, Response.Listener<JSONObject> onSuccess, Response.ErrorListener onError) throws JSONException {
        JsonObjectRequest request = new EmptyResponseJsonObjectRequest(
                Request.Method.PATCH,
                URL + "/interventions/"+interventionID+"/pointsOfInterest/"+poi.id+"/move",
                Mapper.pointOfInterestToJSON(poi),
                onSuccess,
                onError);
        requestQueue.add(request);
    }
}

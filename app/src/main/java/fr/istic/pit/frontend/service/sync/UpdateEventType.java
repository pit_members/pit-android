package fr.istic.pit.frontend.service.sync;

/**
 * Énumération pour le type d'évènement de mise à jour,
 * utilisée pour la synchronisation entre tablettes.
 */
public enum UpdateEventType {
    /**
     * Une intervention vient d'être créée
     */
    INTERV_CREATE(true, false),

    /**
     * Une intervention vient d'être clôturée
     */
    INTERV_DISABLE(true, false),

    /**
     * Une demande de moyen vient d'être envoyée
     */
    RESOURCE_REQUEST_CREATE(true, true),

    /**
     * Une demande de moyen vient d'être acceptée par le CODIS
     */
    RESOURCE_REQUEST_ACCEPT(true, true),

    /**
     * Une demande de moyen vient d'être refusée par le CODIS
     */
    RESOURCE_REQUEST_REJECT(true, true),

    /**
     * Un moyen vient d'être libéré
     */
    RESOURCE_RELEASE(false, true),

    /**
     * Un moyen vient d'être déplacé sur site / dans le CRM
     */
    RESOURCE_MOVE(false, true),

    /**
     * Un moyen vient d'arriver sur site / à l'emplacement demandé
     */
    RESOURCE_ARRIVE(false, true),

    /**
     * Un moyen vient d'être modifié (changement de rôle)
     */
    RESOURCE_UPDATE(false, true),

    /**
     * Un point d'intérêt vient d'être ajouté sur une intervention
     */
    POI_CREATE(false, true),

    /**
     * Un point d'intérêt vient d'être supprimé d'une intervention
     */
    POI_DISABLE(false, true),

    /**
     * Un point d'intérêt vient d'être déplacé
     */
    POI_UPDATE(false, true),

    /**
     * Un drone vient de s'enregistrer sur une intervention
     */
    REGISTERED_DRONE(false, true),

    /**
     * Une mission de drone vient d'être créée
     */
    MISSION_CREATE(false, true),

    /**
     * Le dernier état du drone vient d'être reçu
     */
    DRONE_STATE(false, true),

    /**
     * Le drone vient de signaler un atterrissage d'urgence
     */
    EMERGENCY_RTL(false, true);


    /**
     * Ce type d'évènement doit-il être diffusé sur le canal global ?
     */
    private final boolean global;

    /**
     * Ce type d'évènement doit-il être diffusé sur le canal spécifique à l'intervention concernée ?
     */
    private final boolean perInterv;

    /**
     * Constructeur de base
     *
     * @param global    L'évènement est global
     * @param perInterv L'évènement est spécifique à une intervention
     */
    UpdateEventType(boolean global, boolean perInterv) {
        this.global = global;
        this.perInterv = perInterv;
    }

    /**
     * Indique si l'évènement est global.
     *
     * @return true si l'évènement est global, false sinon
     */
    public boolean isGlobal() {
        return global;
    }

    /**
     * Indique si l'évènement est spécifique à une intervention
     *
     * @return true si l'évènement est spécifique, false sinon
     */
    public boolean isPerInterv() {
        return perInterv;
    }
}

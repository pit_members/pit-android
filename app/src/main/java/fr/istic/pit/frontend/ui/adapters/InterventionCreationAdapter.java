package fr.istic.pit.frontend.ui.adapters;

import android.app.AlertDialog;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import fr.istic.pit.frontend.R;

/**
 * Liste des moyens alloués pour cette intervention
 */
public class InterventionCreationAdapter extends RecyclerView.Adapter<InterventionCreationAdapter.MyViewHolder> {

    private List<Pair<String, String>> resources = new ArrayList<>();

    @Override
    public int getItemCount() {
        return resources.size();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.activity_intervention_creation_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Pair<String, String> pair = resources.get(position);
        holder.display(pair);
    }

    private void removeAt(int position) {
        resources.remove(position);
        this.notifyDataSetChanged();
    }

    public void newItem(Pair<String, String> item) {
        resources.add(0, item);
        this.notifyDataSetChanged();
    }

    public List<Pair<String, String>> getResources() {
        return this.resources;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        private final TextView name;
        private final TextView description;

        private Pair<String, String> currentPair;

        MyViewHolder(final View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.name);
            description = itemView.findViewById(R.id.type);
            Button deleteButton = itemView.findViewById(R.id.deleteButton);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new AlertDialog.Builder(itemView.getContext())
                            .setTitle(currentPair.first)
                            .setMessage(currentPair.second)
                            .show();
                }
            });

            deleteButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    InterventionCreationAdapter.this.removeAt(getAdapterPosition());
                }
            });
        }

        void display(Pair<String, String> pair) {
            currentPair = pair;
            name.setText(pair.first);
            description.setText(pair.second);
        }
    }
}
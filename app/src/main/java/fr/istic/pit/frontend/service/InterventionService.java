package fr.istic.pit.frontend.service;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import fr.istic.pit.frontend.model.Intervention;
import fr.istic.pit.frontend.model.Mapper;

public class InterventionService extends AbstractService {

    public static InterventionService getService(Context context) {
        return new InterventionService(context);
    }

    protected InterventionService(Context context) {
        super(context);
    }

    public void getListInterventions(Response.Listener<JSONArray> onSuccess, Response.ErrorListener onError) {
        JsonArrayRequest request = new EmptyResponseJsonArrayRequest(
                Request.Method.GET,
                URL + "/interventions",
                null,
                onSuccess,
                onError);
        requestQueue.add(request);
    }

    public void getIntervention(Long interventionId, Response.Listener<JSONObject> onSuccess, Response.ErrorListener onError) {
        JsonObjectRequest request = new EmptyResponseJsonObjectRequest(
                Request.Method.GET,
                URL + "/interventions/"+interventionId,
                null,
                onSuccess,
                onError);
        requestQueue.add(request);
    }

    public void createIntervention(Intervention intervention, boolean isCodis, Response.Listener<JSONObject> onSuccess, Response.ErrorListener onError) throws JSONException {
        JSONObject interventionJSON = Mapper.interventionToJSON(intervention);
        interventionJSON.remove("pointsOfInterest");

        JsonObjectRequest request = new EmptyResponseJsonObjectRequest(
                Request.Method.POST,
                URL + "/interventions",
                interventionJSON,
                onSuccess,
                onError,
                isCodis);
        requestQueue.add(request);
    }
}

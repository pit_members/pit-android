package fr.istic.pit.frontend.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import fr.istic.pit.frontend.R;

/**
 * Fragment représentant le CRM d'une intervention. Permet de découper le code du layout de {@link SitacViewFragment} pour lisibilité
 *
 */
public class InterventionCRMFragment extends Fragment {

    public InterventionCRMFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_intervention_crm, container, false);
    }
}

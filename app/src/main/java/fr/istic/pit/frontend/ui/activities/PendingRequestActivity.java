package fr.istic.pit.frontend.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import android.os.Bundle;

import butterknife.ButterKnife;
import fr.istic.pit.frontend.R;
import fr.istic.pit.frontend.ui.fragments.PendingRequestFragment;

/**
 * Activity listant les demandes de moyens pour toutes les interventions (le CODIS seulement y a accès pour validation ou refus de la demande)
 * Elle contient seulement un fragment {@link PendingRequestFragment}
 */
public class PendingRequestActivity extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_request);
        ButterKnife.bind(this);

        Fragment pendingRequestFragment = new PendingRequestFragment();
        pendingRequestFragment.setArguments(getIntent().getExtras());

        getSupportFragmentManager().beginTransaction()
                .add(R.id.pendingRequestFragmentReference, pendingRequestFragment).commit();

    }
}

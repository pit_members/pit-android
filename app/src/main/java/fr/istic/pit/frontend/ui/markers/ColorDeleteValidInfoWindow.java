package fr.istic.pit.frontend.ui.markers;

import android.widget.ImageView;

import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.infowindow.InfoWindow;

import fr.istic.pit.frontend.R;
import fr.istic.pit.frontend.ui.fragments.SitacViewFragment;

/**
 * Infowindow permettant de supprimer, changer le rôle ou valider la position d'une ressource
 */
public class ColorDeleteValidInfoWindow extends InfoWindow {

    private POIMarker marker;
    private SitacViewFragment sitacViewFragment;

    public ColorDeleteValidInfoWindow(MapView mapView, SitacViewFragment sitacViewFragment, POIMarker marker) {
        super(R.layout.infowindows_color_delete_valid, mapView);
        this.marker = marker;
        this.sitacViewFragment = sitacViewFragment;
    }

    @Override
    public void onClose() {
        // méthode non utile
    }

    @Override
    public void onOpen(Object item) {
        final ImageView delete = mView.findViewById(R.id.deleteMarker);
        final ImageView color = mView.findViewById(R.id.colorMarker);
        final ImageView valid = mView.findViewById(R.id.validMarker);
        delete.setOnClickListener(InfoWindowUtils.getDeleteListener(sitacViewFragment, marker, this, sitacViewFragment.folderResources));
        color.setOnClickListener(InfoWindowUtils.getColorListener(sitacViewFragment, marker, this));
        valid.setOnClickListener(InfoWindowUtils.getValidateListener(sitacViewFragment, marker, this));
    }

}

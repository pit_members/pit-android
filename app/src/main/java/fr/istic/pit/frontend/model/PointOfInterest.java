package fr.istic.pit.frontend.model;

import org.osmdroid.util.GeoPoint;

/**
 * Classe DTO pour les points d'intérêts
 */
public class PointOfInterest {
    /**
     * Latitude
     */
    public Double latitude;

    /**
     * Longitude
     */
    public Double longitude;

    /**
     * Un code identifiant le type de pictogramme à utiliser
     */
    public String type;

    /**
     * Id du POI
     */
    public long id;

    /**
     * Id de l'intervention associé au POI
     */
    public long intervId;

    public PointOfInterest(GeoPoint point, String type) {
        this.latitude = point.getLatitude();
        this.longitude = point.getLongitude();
        this.type = type;
    }

    public PointOfInterest() {

    }
}

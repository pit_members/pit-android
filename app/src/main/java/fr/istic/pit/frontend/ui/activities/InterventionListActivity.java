package fr.istic.pit.frontend.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONException;

import butterknife.BindView;
import butterknife.ButterKnife;
import fr.istic.pit.frontend.R;
import fr.istic.pit.frontend.model.Intervention;
import fr.istic.pit.frontend.model.Mapper;
import fr.istic.pit.frontend.service.sync.SynchroClientService;
import fr.istic.pit.frontend.service.sync.UpdateEventType;
import fr.istic.pit.frontend.ui.adapters.InterventionListAdapter;
import fr.istic.pit.frontend.service.InterventionService;

/**
 * Vue liste des interventions
 */
public class InterventionListActivity extends AppCompatActivity implements InterventionListAdapter.InterventionListener {

    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    InterventionListAdapter interventionListAdapter;
    InterventionService interventionService;
    private SynchroClientService synchroClient;
    boolean isCodis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intervention_list);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        isCodis = intent.getBooleanExtra("isCodis", false);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        interventionListAdapter = new InterventionListAdapter(this);
        recyclerView.setAdapter(interventionListAdapter);
        interventionService = InterventionService.getService(this);
        synchroClient = SynchroClientService.getService(this);

        loadInterventions();
        syncInit();
    }

    @Override
    public void onGoToInterventionClicked(Intervention intervention) {
        Intent nextActivity = new Intent(InterventionListActivity.this, InterventionViewActivity.class);
        nextActivity.putExtra("interventionId", intervention.id);
        nextActivity.putExtra("isCodis", isCodis);
        nextActivity.putExtra("sinisterCode", intervention.sinisterCode);
        startActivity(nextActivity);
    }

    private void loadInterventions() {
        interventionService.getListInterventions(
                response -> {
                    try {
                        interventionListAdapter.setInterventions(Mapper.parseIntervention(response));
                    } catch (JSONException e) {
                        Toast.makeText(InterventionListActivity.this, "Erreur de format des interventions obtenues.", Toast.LENGTH_LONG).show();
                    }
                },
                error -> Toast.makeText(InterventionListActivity.this, error.toString(), Toast.LENGTH_LONG).show()
        );
    }

    private void syncInit() {
        // Evènements globaux
        synchroClient.subscribeTo(event -> {
            UpdateEventType type = event.getType();
            if(
                type.equals(UpdateEventType.INTERV_DISABLE)||
                type.equals(UpdateEventType.INTERV_CREATE)
            ) {
                loadInterventions();
            }
        });
    }
}

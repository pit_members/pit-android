package fr.istic.pit.frontend.ui.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Switch;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fr.istic.pit.frontend.R;
import fr.istic.pit.frontend.model.Drone;
import fr.istic.pit.frontend.model.DronePoint;
import fr.istic.pit.frontend.ui.activities.DronePicturesActivity;
import fr.istic.pit.frontend.ui.adapters.DroneParcoursListAdapter;

/**
 * Fragment pour la liste des {@link DronePoint} du parcours et les boutons d'actions pour manipuler le drone
 */
public class DroneParcoursFragment extends Fragment implements DroneParcoursListAdapter.DroneLocationListListener{

    @BindView(R.id.droneParcoursFragmentRecyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.droneParcoursFragmentButtonAddPoint)
    Button addButton;

    @BindView(R.id.droneParcoursFragmentButtonPause)
    Button pauseResumeButton;

    @BindView(R.id.droneParcoursFragmentButtonStop)
    Button stopButton;

    @BindView(R.id.switchBoucleOuverte)
    Switch boucleOuverte;

    private DroneParcoursListAdapter adapter;
    private DroneParcoursEventListener listener;

    private long interventionId = 0L;

    public DroneParcoursFragment() {
        // Required empty public constructor
    }

    DroneParcoursFragment(DroneParcoursEventListener listener, long interventionId) {
        this.listener = listener;
        this.interventionId = interventionId;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_drone_parcours, container, false);
        ButterKnife.bind(this,v);

        adapter = new DroneParcoursListAdapter(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
        return v;
    }

    /**
     * Mets à jour les items de l'adapter de la recyclerView
     */
    public void updatePoints() {
        adapter.setPoints(listener.getPoints());
    }

    /**
     * Vide les items de l'adapter de la recyclerView
     */
    public void clearPoints() {
        adapter.setPoints(Collections.emptyList());
    }

    /**
     * Change la position d'un point dans la liste des points du parcours
     * @param dronePoint le point à déplacer
     * @param newPosition la nouvelle position du point dans la liste
     */
    @Override
    public void onChangePosition(DronePoint dronePoint, int newPosition) {
        listener.getPoints().remove(dronePoint);
        listener.getPoints().add(newPosition, dronePoint);

        updatePointsOrder();

        adapter.setPoints(listener.getPoints());
        recyclerView.scrollToPosition(newPosition);
    }

    /**
     * Lance l'activité {@link DronePicturesActivity} pour afficher les photos prises sur le point en paramètre
     * @param dronePoint le point pour lequel on veut voir les photos
     */
    @Override
    public void viewPictures(DronePoint dronePoint) {
        Intent nextActivity = new Intent(getContext(), DronePicturesActivity.class);
        nextActivity.putExtra("interventionId", interventionId);
        nextActivity.putExtra("latitude", dronePoint.latitude);
        nextActivity.putExtra("longitude", dronePoint.longitude);
        startActivity(nextActivity);
    }

    /**
     * Appelé lorsqu'un point du parcours est supprimé via l'adapter
     * Supprime un point, mets à jour la liste de l'adapter, et mets à jour l'ui
     * @param dronePoint le point supprimé
     */
    @Override
    public void onDeletePoint(DronePoint dronePoint) {
        listener.getPoints().remove(dronePoint);
        adapter.setPoints(listener.getPoints());
        listener.onDeletePoint(dronePoint);
        updatePointsOrder();
        updateDisplayedButtons();
    }

    /**
     * Permet de savoir la mission est en cours d'édition
     * @return true si en cours d'édition
     */
    @Override
    public boolean isEditing() {
        return listener.isEditing();
    }

    /**
     * Cache le fragment
     */
    @OnClick(R.id.droneParcoursFragmentHide)
    void hide(){
        listener.onCloseFragment();
    }

    /**
     * Arrête la mission
     */
    @OnClick(R.id.droneParcoursFragmentButtonStop)
    void stop(){
        listener.onMissionStop();
        updateDisplayedButtons(true);
    }

    /**
     * Pause ou relance la mission
     */
    @OnClick(R.id.droneParcoursFragmentButtonPause)
    void startPause(){
        listener.onMissionStartPause();
        updateDisplayedButtons();
    }

    /**
     * Ajoute un nouveau point au parcours au centre de la carte
     */
    @OnClick(R.id.droneParcoursFragmentButtonAddPoint)
    void newPoint(){
        listener.onMissionAddPoint();
        adapter.setPoints(listener.getPoints());
        updateDisplayedButtons();
    }

    /**
     * Update un booléen lorsque qu'on switch entre un parcours en boucle ou aller-retour
     */
    @OnClick(R.id.switchBoucleOuverte)
    void onSwitchBoucleOuverte() {
        listener.setMissionLoop(!boucleOuverte.isChecked());
    }

    /**
     * Met à jour la valeur du champ {@link DronePoint#orderNumber} lorsque l'ordre d'un des points du parcours est changé
     */
    private void updatePointsOrder(){
        for(DronePoint point : listener.getPoints()){
            point.orderNumber=listener.getPoints().indexOf(point);
        }
        listener.onNewOrder();
    }

    /**
     * Mets à jour les boutons d'action affichés
     */
    public void updateDisplayedButtons() {
        updateDisplayedButtons(false);
    }

    /**
     * Mets à jour les boutons d'action affichés
     * @param forceBegin force l'affichage de manière à indiquer que c'est le début d'une nouvelle mission (drone à terre)
     */
    public void updateDisplayedButtons(boolean forceBegin){
        if(listener.isEditable()) {
            pauseResumeButton.setVisibility(View.VISIBLE);
            if(listener.isEditing()) {
                addButton.setVisibility(View.VISIBLE);
                stopButton.setVisibility(!forceBegin && !Drone.State.GROUND.equals(listener.getDroneState()) ? View.VISIBLE : View.GONE);
                pauseResumeButton.setText(forceBegin || Drone.State.GROUND.equals(listener.getDroneState()) ? "Lancer" : "Recommencer");
                pauseResumeButton.setEnabled(listener.getPoints().size() >= 2);
		        boucleOuverte.setVisibility(View.VISIBLE);
            } else {
                addButton.setVisibility(View.GONE);
                stopButton.setVisibility(View.VISIBLE);
                pauseResumeButton.setText("Stop");
                boucleOuverte.setVisibility(View.GONE);
            }
        } else {
            addButton.setVisibility(View.GONE);
            pauseResumeButton.setVisibility(View.GONE);
            stopButton.setVisibility(View.GONE);
            boucleOuverte.setVisibility(View.GONE);
        }
        adapter.notifyDataSetChanged();
    }

    /**
     * Interface permettant l'interaction entre ce fragment et {@link DroneFragment}
     */
    public interface DroneParcoursEventListener {
        /**
         * Appelé lorsque l'ordre des points du parcours est changé
         */
        void onNewOrder();

        /**
         * Appelé lorsque le fragment est caché
         */
        void onCloseFragment();

        /**
         * Récupère la liste des points du parcours
         * @return une liste de points
         */
        List<DronePoint> getPoints();

        /**
         * Appelé lorsqu'on veut arrêter ou relancer la mission du drone
         */
        void onMissionStartPause();

        /**
         * Appelé pour arrêter le drone et le retourner à sa base de lancement
         */
        void onMissionStop();

        /**
         * Appelé lorsqu'un point du parcours est supprimé
         * @param dronePoint le point supprimé
         */
        void onDeletePoint(DronePoint dronePoint);

        /**
         * Ajoute un nouveau point au parcours au centre de la carte
         */
        void onMissionAddPoint();

        /**
         * Permet de savoir si la mission est éditable
         * @return true si éditable
         */
        boolean isEditable();

        /**
         * Permet de savoir la mission est en cours d'édition
         * @return true si en cours d'édition
         */
        boolean isEditing();

        /**
         * Permet de récupérer l'état du drone
         * @return l'état du drone
         */
        Drone.State getDroneState();

        /**
         * Définit si la mission est en aller-retour (mission ouverte) ou en boucle
         * @param missionLoop true si la mission boucle
         */
        void setMissionLoop(boolean missionLoop);
    }
}

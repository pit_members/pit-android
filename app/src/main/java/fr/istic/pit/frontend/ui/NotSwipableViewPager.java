package fr.istic.pit.frontend.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

/**
 * ViewPager empêchant l'utilisateur de swiper d'un onglet à un autre
 */
public class NotSwipableViewPager extends ViewPager {
    public NotSwipableViewPager(@NonNull Context context) {
        super(context);
    }

    public NotSwipableViewPager(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return false;
    }
}

package fr.istic.pit.frontend.ui.fragments;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import fr.istic.pit.frontend.R;
import fr.istic.pit.frontend.model.Intervention;
import fr.istic.pit.frontend.model.Mapper;
import fr.istic.pit.frontend.model.Resource;
import fr.istic.pit.frontend.service.InterventionService;
import fr.istic.pit.frontend.service.RessourceService;
import fr.istic.pit.frontend.service.sync.SynchroClientService;
import fr.istic.pit.frontend.service.sync.UpdateEventType;
import fr.istic.pit.frontend.ui.activities.InterventionViewActivity;
import fr.istic.pit.frontend.ui.adapters.PendingRequestGlobalAdapter;


/**
 * Fragment affichant les demandes en attentes. Visible uniquement par le CODIS
 */
public class PendingRequestFragment extends Fragment implements PendingRequestGlobalAdapter.PendingRequestGlobalListener {

    @BindView(R.id.demandeListRecyclerView)RecyclerView recyclerView;
    @BindView(R.id.demandeListMapButton)TextView mapListButton;
    private PendingRequestGlobalAdapter adapter;

    RessourceService ressourceService;
    InterventionService interventionService;
    private SynchroClientService synchroClient;

    private boolean isCodis;
    private long interventionId;

    public PendingRequestFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_pending_request, container, false);

        ButterKnife.bind(this,v);

        if (getArguments() != null) {
            isCodis = getArguments().getBoolean("isCodis",false);
            interventionId = getArguments().getLong("interventionId");
        }

        ressourceService = RessourceService.getService(getContext());
        interventionService = InterventionService.getService(getContext());
        synchroClient = SynchroClientService.getService(getContext());

        adapter = new PendingRequestGlobalAdapter(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);

        if(interventionId!=0L){
            adapter.setDisplayMapButton(false);
            mapListButton.setVisibility(View.GONE);
        }

        getWaitingRessources();
        syncInit();

        return v;
    }

    /**
     * Récupère les demandes de moyens.
     * Récupère seulement les demandes liées à l'intervention si la variable a été valorisée via les arguments
     */
    private void getWaitingRessources() {
        if(interventionId==0L) {
            ressourceService.getRessourceListByState(
                "WAITING",
                null,
                response -> {
                    try {
                        List<Resource> requests = Mapper.parseResource(response);
                        adapter.setRequests(requests);
                        getInterventionsRelatedToRequests(requests);
                    } catch (JSONException e) {
                        Toast.makeText(getContext(), "Erreur de format des ressources obtenues.", Toast.LENGTH_LONG).show();
                    }
                },
                error -> Toast.makeText(getContext(), error.toString(), Toast.LENGTH_LONG).show()
            );
        }else{
            ressourceService.getRessourceListByInterventionId(
                interventionId,
                response -> {
                    try {
                        List<Resource> tmp = Mapper.parseResource(response);
                        List<Resource> requests = new ArrayList<>();
                        for(Resource res : tmp){
                            if("WAITING".equals(res.macroState)){
                                requests.add(res);
                            }
                        }
                        adapter.setRequests(requests);
                        getInterventionsRelatedToRequests(requests);
                    }catch(JSONException e){
                        Toast.makeText(getContext(), "Erreur de format des ressources obtenues.", Toast.LENGTH_LONG).show();
                    }
                },
                error -> Toast.makeText(getContext(), error.toString(), Toast.LENGTH_LONG).show()
            );
        }
    }

    /**
     * Récupère les interventions pour chaque moyen demandé afin d'afficher le nom dans la liste des demandes
     * @param requests
     */
    private void getInterventionsRelatedToRequests(List<Resource> requests){
        for(Resource resource : requests){
            interventionService.getIntervention(
                    resource.intervId,
                    response -> {
                        try {
                            Intervention intervention = Mapper.parseIntervention(response);
                            adapter.addIntervention(intervention);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    },
                    error -> {});
        }
    }

    /**
     * Lance l'activité {@link InterventionViewActivity}
     * @param resource le moyen dont on veut voir l'intervention
     */
    @Override
    public void onGoToIntervention(Resource resource) {
        Intent nextActivity = new Intent(getContext(), InterventionViewActivity.class);
        nextActivity.putExtra("isCodis", isCodis);
        nextActivity.putExtra("interventionId", resource.intervId);
        startActivity(nextActivity);
    }

    /**
     * Lance une AlertDialog pour spécifier le nom du moyen qu'on veut accepter
     * @param resource le moyen à nommer et accepter
     */
    @Override
    public void onAcceptRequest(Resource resource) {
        final EditText edittext = new EditText(getContext());
        edittext.setHint("nom du moyen");
        edittext.setSingleLine();
        new AlertDialog.Builder(getContext())
                .setTitle("Nom du moyen")
                .setView(edittext)
                .setPositiveButton(R.string.add, (dialog, id) -> {
                    if(StringUtils.isNotBlank(edittext.getText().toString())) {
                        String name = edittext.getText().toString();
                        ressourceService.acceptRessource(
                                resource.intervId,
                                resource.id,
                                isCodis,
                                name,
                                response -> getWaitingRessources(),
                                error -> Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_LONG).show());
                    }else{
                        Toast.makeText(getContext(),"Veuillez préciser le nom du moyen", Toast.LENGTH_SHORT).show();
                    }
                })
                .setNegativeButton(R.string.cancel, (dialog, id) -> {
                }).show();
    }

    /**
     * Rejette une demande de moyen
     * @param resource le moyen rejeté
     */
    @Override
    public void onRejectRequest(Resource resource) {
        ressourceService.rejectRessource(
                resource.intervId,
                resource.id,
                isCodis,
                response -> getWaitingRessources(),
                error -> {});
    }

    /**
     * Souscrit le fragment à la synchronisation et spécifie les actions à effectuer lors de la réception d'un message
     */
    private void syncInit() {
        // Evènements globaux
        synchroClient.subscribeTo(event -> {
            UpdateEventType type = event.getType();
            if(
                type.equals(UpdateEventType.RESOURCE_REQUEST_CREATE)||
                type.equals(UpdateEventType.RESOURCE_REQUEST_ACCEPT)||
                type.equals(UpdateEventType.RESOURCE_REQUEST_REJECT)
            ) {
                getWaitingRessources();
                Toast.makeText(getContext(),"Demandes de moyen mises à jour", Toast.LENGTH_SHORT).show();
            }
        });
    }
}

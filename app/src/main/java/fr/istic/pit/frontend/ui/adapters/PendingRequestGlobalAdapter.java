package fr.istic.pit.frontend.ui.adapters;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.istic.pit.frontend.R;
import fr.istic.pit.frontend.model.Intervention;
import fr.istic.pit.frontend.model.Resource;

/**
 * Adapter lié à un RecyclerView pour afficher la liste de demandes de moyen
 */
public class PendingRequestGlobalAdapter extends RecyclerView.Adapter<PendingRequestGlobalAdapter.IconViewHolder> {

    private List<Resource> requests = new ArrayList<>();
    private Map<Long, Intervention> interventions = new HashMap<>();
    private PendingRequestGlobalListener listener;
    private boolean displayMapButton = true;

    public interface PendingRequestGlobalListener{
        void onGoToIntervention(Resource resourceTO);
        void onAcceptRequest(Resource resourceTO);
        void onRejectRequest(Resource resourceTO);
    }

    public PendingRequestGlobalAdapter(PendingRequestGlobalListener listener){
        this.listener=listener;
    }

    public class IconViewHolder extends RecyclerView.ViewHolder {
        TextView demandeInterventionID;
        TextView demandeType;
        Button acceptButton;
        Button refuseButton;
        Button mapButton;
        public ConstraintLayout layout;

        IconViewHolder(View v) {
            super(v);
            demandeInterventionID = v.findViewById(R.id.demandeItemInterventionId);
            demandeType = v.findViewById(R.id.demandeItemType);
            acceptButton = v.findViewById(R.id.demandeItemAcceptButton);
            refuseButton = v.findViewById(R.id.demandeItemRefuseButton);
            mapButton = v.findViewById(R.id.demandeItemMapButton);
            layout = v.findViewById(R.id.demandeItem);

            acceptButton.setOnClickListener(view -> listener.onAcceptRequest(requests.get(getAdapterPosition())));
            refuseButton.setOnClickListener(view -> listener.onRejectRequest(requests.get(getAdapterPosition())));
            mapButton.setOnClickListener(view -> listener.onGoToIntervention(requests.get(getAdapterPosition())));

            if(!displayMapButton){
                mapButton.setVisibility(View.GONE);
            }
        }
    }

    /**
     * Permet de choisir d'afficher ou non les boutons pour accéder à la vue sitac des interventions liées aux moyens
     * @param displayMapButton , true si on veut afficher le bouton de la carte
     */
    public void setDisplayMapButton(boolean displayMapButton){
        this.displayMapButton = displayMapButton;
    }

    public void setRequests(List<Resource> requests){
        this.requests = requests;
        notifyDataSetChanged();
    }

    public void addIntervention(Intervention inter){
        interventions.put(inter.id,inter);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public IconViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pending_request_global_item, parent, false);
        return new IconViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull IconViewHolder holder, int position) {
        //TODO
        Resource d = requests.get(position);
        if(null!=interventions.get(d.intervId)) {
            holder.demandeInterventionID.setText(interventions.get(d.intervId).name);
        }else{
            holder.demandeInterventionID.setText("Intervention #loading");
        }
        holder.demandeType.setText(d.type);
        holder.acceptButton.setText("Accepter");
        holder.refuseButton.setText("Refuser");
        holder.mapButton.setText("Voir intervention");
        if(position%2==0){
            holder.layout.setBackgroundColor(Color.parseColor("#666666"));
        }else{
            holder.layout.setBackgroundColor(Color.parseColor("#606060"));
        }
    }

    @Override
    public int getItemCount() {
        return requests.size();
    }

}

package fr.istic.pit.frontend.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;

import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fr.istic.pit.frontend.R;
import fr.istic.pit.frontend.Utils;
import fr.istic.pit.frontend.model.Resource;
import fr.istic.pit.frontend.service.RessourceService;

/**
 * Activity permettant de faire une demande de moyen
 * Requiert l'id de l'intervention "interventionId" et son code sinistre "sinisterCode" à passer en extra dans l'Intent
 */
public class RequestResourcesActivity extends AppCompatActivity {

    @BindView(R.id.requestFPTTextView)TextView quantityOfFPT;
    @BindView(R.id.requestVSAVTextView)TextView quantityOfVSAV;
    @BindView(R.id.requestVLCGTextView)TextView quantityOfVLCG;

    @BindView(R.id.requestMinusFPTButton)Button minusFPT;
    @BindView(R.id.requestMinusVSAVButton)Button minusVSAV;
    @BindView(R.id.requestMinusVLCGButton)Button minusVLCG;

    @BindView(R.id.requestVLCGImage)ImageView requestVlcgIcon;
    @BindView(R.id.requestVLCGImageText)TextView requestVlcgText;

    private long interventionId;
    private RessourceService resourceService;

    private int quantityFPTint=0;
    private int quantityVSAVint=0;
    private int quantityVLCGint=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_resources);
        ButterKnife.bind(this);

        interventionId = getIntent().getLongExtra("interventionId",-1);
        if(interventionId==-1){
            Toast.makeText(this,"L'ID de l'intervention n'a pas été renseigné",Toast.LENGTH_SHORT).show();
            finish();
        }

        quantityOfFPT.setText(String.valueOf(quantityFPTint));
        quantityOfVLCG.setText(String.valueOf(quantityVLCGint));
        quantityOfVSAV.setText(String.valueOf(quantityVSAVint));

        minusFPT.setEnabled(false);
        minusVSAV.setEnabled(false);
        minusVLCG.setEnabled(false);

        displayVlcgIcon();

        resourceService = RessourceService.getService(this);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
        int height = dm.heightPixels;
        getWindow().setLayout((int)(width*.5),(int)(height*.6));
    }

    /**
     * Change la couleur de l'icone de vlcg suivant le code sinistre
     * Passe la couleur à vert si le code est "ALP"
     */
    private void displayVlcgIcon() {
        int vlcgIcon = R.drawable.ic_vehicule_actif_feu;
        String textColor = "#770000";
        String sinisterCode = getIntent().getStringExtra("sinisterCode");
        if (sinisterCode != null) {
            if (sinisterCode.equals(Utils.ALP)) {
                vlcgIcon = R.drawable.ic_vehicule_actif_humain;
                textColor = "#007700";
            }
        }
        requestVlcgIcon.setImageDrawable(getDrawable(vlcgIcon));
        requestVlcgText.setTextColor(Color.parseColor(textColor));
    }

    /**
     * Diminue de 1 le nombre de FPT demandé
     */
    @OnClick(R.id.requestMinusFPTButton)
    public void decreaseFPT(){
        quantityFPTint--;
        quantityOfFPT.setText(String.valueOf(quantityFPTint));
        if(quantityFPTint==0){
            minusFPT.setEnabled(false);
        }
    }

    /**
     * Diminue de 1 le nombre de VSAV demandé
     */
    @OnClick(R.id.requestMinusVSAVButton)
    public void decreaseVSAV(){
        quantityVSAVint--;
        quantityOfVSAV.setText(String.valueOf(quantityVSAVint));
        if(quantityVSAVint==0){
            minusVSAV.setEnabled(false);
        }
    }

    /**
     * Diminue de 1 le nombre de VLCG demandé
     */
    @OnClick(R.id.requestMinusVLCGButton)
    public void decreaseVLCG(){
        quantityVLCGint--;
        quantityOfVLCG.setText(String.valueOf(quantityVLCGint));
        if(quantityVLCGint==0){
            minusVLCG.setEnabled(false);
        }
    }

    /**
     * Augmente de 1 le nombre de FPT demandé
     */
    @OnClick(R.id.requestPlusFPTButton)
    public void increaseFPT(){
        quantityFPTint++;
        quantityOfFPT.setText(String.valueOf(quantityFPTint));
        if(quantityFPTint>0){
            minusFPT.setEnabled(true);
        }
    }

    /**
     * Augmente de 1 le nombre de VSAV demandé
     */
    @OnClick(R.id.requestPlusVSAVButton)
    public void increaseVSAV(){
        quantityVSAVint++;
        quantityOfVSAV.setText(String.valueOf(quantityVSAVint));
        if(quantityVSAVint>0){
            minusVSAV.setEnabled(true);
        }
    }

    /**
     * Augmente de 1 le nombre de VLCG demandé
     */
    @OnClick(R.id.requestPlusVLCGButton)
    public void increaseVLCG(){
        quantityVLCGint++;
        quantityOfVLCG.setText(String.valueOf(quantityVLCGint));
        if(quantityVLCGint>0){
            minusVLCG.setEnabled(true);
        }
    }

    /**
     * Envoie une requête de demande de moyen et quitte l'intervention
     */
    @OnClick(R.id.requestSendButton)
    public void send(){
        try {
            List<Resource> resources = new LinkedList<>();
            for (int i = 0; i < quantityFPTint; i++) {
                Resource resource = new Resource();
                resource.latitude = 0.0;
                resource.longitude = 0.0;
                resource.type = "FPT";
                resources.add(resource);
            }
            for (int i = 0; i < quantityVSAVint; i++) {
                Resource resource = new Resource();
                resource.latitude = 0.0;
                resource.longitude = 0.0;
                resource.type = "VSAV";
                resources.add(resource);
            }
            for (int i = 0; i < quantityVLCGint; i++) {
                Resource resource = new Resource();
                resource.latitude = 0.0;
                resource.longitude = 0.0;
                resource.type = "VLCG";
                resources.add(resource);
            }

            resourceService.requestResource(
                    interventionId,
                    resources,
                    response -> finish(),
                    error -> Toast.makeText(this, error.toString(), Toast.LENGTH_LONG).show());
        } catch (JSONException e) {
            Toast.makeText(this, "Erreur lors de la récupération des informations à partir du formulaires.", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Quitte l'intervention
     */
    @OnClick(R.id.requestCancelButton)
    public void cancel(){
        finish();
    }
}

package fr.istic.pit.frontend.service;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import fr.istic.pit.frontend.model.DronePoint;
import fr.istic.pit.frontend.model.Mapper;

public class DroneService extends AbstractService {

    protected DroneService(Context context) {
        super(context);
    }

    public static DroneService getService(Context context) {
        return new DroneService(context);
    }

    public void createMission(Long interventionId, Boolean boucleOuverte, List<DronePoint> dronePoints, Response.Listener<JSONObject> onSuccess, Response.ErrorListener onError) throws JSONException {
        JSONArray points = Mapper.dronePointToJSONPartial(dronePoints);
        JSONObject body = new JSONObject();
        body.put("missionLoop", boucleOuverte);
        body.put("points", points);
        JsonObjectRequest request = new EmptyResponseJsonObjectRequest(
                Request.Method.POST,
                URL + "/interventions/" + interventionId + "/drone/mission",
                body,
                onSuccess,
                onError);
        requestQueue.add(request);
    }

    public void getMission(Long interventionId, Response.Listener<JSONObject> onSuccess, Response.ErrorListener onError) {
        JsonObjectRequest request = new EmptyResponseJsonObjectRequest(
                Request.Method.GET,
                URL + "/interventions/" + interventionId + "/drone/mission",
                null,
                onSuccess,
                onError);
        requestQueue.add(request);
    }

    public void stopMission(Long interventionId, Response.Listener<JSONObject> onSuccess, Response.ErrorListener onError) {
        JsonObjectRequest request = new EmptyResponseJsonObjectRequest(
                Request.Method.PATCH,
                URL + "/interventions/" + interventionId + "/drone/mission/stop",
                null,
                onSuccess,
                onError);
        requestQueue.add(request);
    }

    public void rtlMission(Long interventionId, Response.Listener<JSONObject> onSuccess, Response.ErrorListener onError) {
        JsonObjectRequest request = new EmptyResponseJsonObjectRequest(
                Request.Method.PATCH,
                URL + "/interventions/" + interventionId + "/drone/mission/rtl",
                null,
                onSuccess,
                onError);
        requestQueue.add(request);
    }

    public void getListPhotoName(Long interventionId, Double latitude, Double longitude, Response.Listener<JSONArray> onSuccess, Response.ErrorListener onError) {
        JsonArrayRequest request = new EmptyResponseJsonArrayRequest(
                Request.Method.GET,
                URL + "/interventions/" + interventionId + "/drone/photo?latitude=" + latitude + "&longitude=" + longitude,
                null,
                onSuccess,
                onError);
        requestQueue.add(request);
    }

    public void getPhoto(Long interventionId, Double latitude, Double longitude, String photoName, Response.Listener<String> onSuccess, Response.ErrorListener onError) {
        StringRequest request = new StringRequest(
                Request.Method.GET,
                URL + "/interventions/" + interventionId + "/drone/photo/" + photoName + "?latitude=" + latitude + "&longitude=" + longitude,
                onSuccess,
                onError);
        requestQueue.add(request);
    }

    public void getVideo(Long interventionId, Response.Listener<String> onSuccess, Response.ErrorListener onError) {
        StringRequest request = new StringRequest(
                Request.Method.GET,
                URL + "/interventions/" + interventionId + "/drone/video",
                onSuccess,
                onError);
        requestQueue.add(request);
    }
}

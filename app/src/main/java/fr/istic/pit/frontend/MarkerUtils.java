package fr.istic.pit.frontend;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.Overlay;
import org.osmdroid.views.overlay.infowindow.InfoWindow;

import fr.istic.pit.frontend.model.PointOfInterest;
import fr.istic.pit.frontend.model.Resource;
import fr.istic.pit.frontend.ui.fragments.SitacViewFragment;
import fr.istic.pit.frontend.ui.markers.ColorDeleteInfoWindow;
import fr.istic.pit.frontend.ui.markers.ColorDeleteValidInfoWindow;
import fr.istic.pit.frontend.ui.markers.DeleteInfoWindow;
import fr.istic.pit.frontend.ui.markers.POIMarker;
import fr.istic.pit.frontend.ui.markers.POIMarkerWithLabel;

/**
 * Classe utilitaire pour les markers
 */
public class MarkerUtils {

    private SitacViewFragment sitacViewFragment;
    private Context mContext;

    public MarkerUtils(SitacViewFragment sitacViewFragment, Context mContext) {
        this.sitacViewFragment = sitacViewFragment;
        this.mContext = mContext;
    }

    /**
     * Met à jour le caractère draggable des markers de la map en fonction d'un booléen
     * @param b un booléen
     */
    public void setMarkersDraggable(boolean b) {
        for(Overlay marker : sitacViewFragment.folderPOI.getItems()){
            if(marker instanceof Marker) {
                ((Marker)marker).setDraggable(b);
            }
        }
        for(Overlay marker : sitacViewFragment.folderResources.getItems()){
            if(marker instanceof Marker) {
                ((Marker)marker).setDraggable(b);
            }
        }
    }

    /**
     * Crée un marker associé à une ressource
     * @param map la MapView
     * @param resource la ressource associée au marker
     * @return un marker
     */
    public Marker createMarkerwithLabel(MapView map, Resource resource) {
        String pictoType = POIUtils.getResourcePictoTypeFromRole(resource.role, resource.microState, resource.macroState, resource.crm);
        Drawable icon = mContext.getDrawable(POIUtils.getDrawable(pictoType));
        GeoPoint position = new GeoPoint(resource.latitude, resource.longitude);
        POIMarkerWithLabel marker = new POIMarkerWithLabel(map,resource);
        marker.setPosition(position);
        marker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
        marker.setIcon(icon);
        sitacViewFragment.folderResources.getItems().add(marker);

        InfoWindow infowindow = getInfoWindow(map, marker);
        marker.setInfoWindow(infowindow);

        marker.setOnMarkerClickListener(closeInfoWindowOnClickMap());
        marker.setOnMarkerDragListener(new Marker.OnMarkerDragListener() {

            @Override
            public void onMarkerDrag(Marker marker) {
                // méthode non utile
            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                launchCancelableToast(marker);
            }

            @Override
            public void onMarkerDragStart(Marker marker) {
                ((POIMarker)marker).setOldPosition(marker.getPosition());
            }
        });
        return marker;
    }

    /**
     * Crée un marker associé à un poi
     * @param map la MapView
     * @param poi le poi associé au marker
     * @return un marker
     */
    public Marker createMarker(MapView map, PointOfInterest poi) {
        Drawable icon = mContext.getDrawable(POIUtils.getDrawable(poi.type));
        GeoPoint position = new GeoPoint(poi.latitude, poi.longitude);
        POIMarker marker = new POIMarker(map,poi);
        marker.setPosition(position);
        marker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
        marker.setIcon(icon);
        sitacViewFragment.folderPOI.getItems().add(marker);
        InfoWindow infowindow = getInfoWindow(map, marker);
        marker.setInfoWindow(infowindow);

        marker.setOnMarkerClickListener(closeInfoWindowOnClickMap());
        marker.setOnMarkerDragListener(new Marker.OnMarkerDragListener() {

            @Override
            public void onMarkerDrag(Marker marker) {
                // méthode non utile
            }

            @RequiresApi(api = Build.VERSION_CODES.P)
            @Override
            public void onMarkerDragEnd(Marker marker) {
                launchCancelableToast(marker);
            }

            @Override
            public void onMarkerDragStart(Marker marker) {
                ((POIMarker)marker).setOldPosition(marker.getPosition());
            }
        });
        return marker;
    }

    /**
     * Récupère l'infowindow à associer à un {@link POIMarker}
     * Si le marker est un {@link POIMarkerWithLabel}, le choix de l'infowindow est fait en
     * fonction du microstate et du macrostate de la ressource associée
     * @param map la MapView
     * @param marker le marker concerné
     * @return une infowindow
     */
    private InfoWindow getInfoWindow(MapView map, POIMarker marker) {
        if (marker instanceof POIMarkerWithLabel) {
            Resource resource = (Resource) marker.getPoi();
            switch (resource.macroState){
                case POIUtils.COMING:
                    return new ColorDeleteValidInfoWindow(map, sitacViewFragment, marker);
                case POIUtils.ARRIVED:
                    if (resource.microState.equals(POIUtils.IDLE)){
                        return new ColorDeleteInfoWindow(map, sitacViewFragment, marker);
                    }
                    return new ColorDeleteValidInfoWindow(map, sitacViewFragment, marker);
                default:
                    return new ColorDeleteInfoWindow(map, sitacViewFragment, marker);
            }
        }
        return new DeleteInfoWindow(map, sitacViewFragment, marker);
    }

    /**
     * En mode édition, affiche l'infowindow du marker lors d'un click sur le marker
     * @return un event listener qui se déclenche lors d'un click sur le marker
     */
    private Marker.OnMarkerClickListener closeInfoWindowOnClickMap(){
        return (marker, mapView) -> {
            if (sitacViewFragment.isEditEnabled()){
                marker.showInfoWindow();
            }
            return true;
        };
    }

    /**
     * Affiche un toast en bas de l'écran permettant d'annuler le déplacement d'un marker
     * Si le bouton Annuler n'est pas cliqué, le toast disparaît au bout de 3 secondes et le
     * déplacement est enregistré
     * @param marker le marker qui a été déplacé
     */
    private void launchCancelableToast(Marker marker){
        LayoutInflater inflater = sitacViewFragment.getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.pop_up_cancel_moving, null);

        final TextView cancel = v.findViewById(R.id.cancel_moving_clickable);
        SpannableString string = new SpannableString("Annuler");
        string.setSpan(new UnderlineSpan(), 0, string.length(), 0);
        cancel.setText(string);

        View anchorView = sitacViewFragment.getView();

        final PopupWindow pw = new PopupWindow(v, 800,
                android.view.ViewGroup.LayoutParams.WRAP_CONTENT);

        pw.setAnimationStyle(R.style.cancel_toast_animation);
        pw.showAtLocation(anchorView, Gravity.BOTTOM, 0, 100);
        Handler handler = new Handler(Looper.getMainLooper());
        Runnable r = () -> {
            pw.dismiss();
            GeoPoint newPosition = marker.getPosition();

            if (marker instanceof POIMarkerWithLabel){
                POIMarker m = (POIMarker)marker;
                Resource resource = (Resource)m.getPoi();
                resource.latitude = newPosition.getLatitude();
                resource.longitude = newPosition.getLongitude();
                sitacViewFragment.updateResourceLocation(resource);
            } else {
                PointOfInterest poi = ((POIMarker)marker).getPoi();
                poi.latitude = newPosition.getLatitude();
                poi.longitude = newPosition.getLongitude();
                sitacViewFragment.updatePOILocation(poi);
            }
        };
        handler.postDelayed(r, 3000);

        cancel.setOnClickListener(v1 -> {
            handler.removeCallbacks(r);
            marker.setPosition(((POIMarker)marker).getOldPosition());
            sitacViewFragment.getMap().invalidate();
            pw.getContentView().setVisibility(View.GONE);
        });
    }
}

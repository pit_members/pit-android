package fr.istic.pit.frontend.ui.fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;

import android.util.Base64;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import fr.istic.pit.frontend.R;
import fr.istic.pit.frontend.service.DroneService;

/**
 * Fragment pour l'affichage vidéo de ce que voit le drone
 */
public class DroneVideoFragment extends Fragment {

    private ImageView videoView;
    private byte[] decodedImage;
    private Bitmap decodedByte;

    private DroneService droneService;
    private long interventionId;

    private MutableLiveData<Pair<Boolean, Boolean>> controlQuery;


    public DroneVideoFragment() {
        // Required empty public constructor
    }

    public DroneVideoFragment(long interventionId) {
        this.interventionId = interventionId;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_drone_video, container, false);
        videoView = v.findViewById(R.id.fragmentVideoImageView);
        droneService = DroneService.getService(getContext());

        controlQuery = new MutableLiveData<>();
        // First = est ce que le fragment est ouvert? / Second = est ce que le fragment est en train de récupérer une frame
        controlQuery.setValue(new Pair<>(false, false));

        controlQuery.observe(this, value -> {
            if(value.first && !value.second)
                display();
        });

        return v;
    }

    /**
     * Récupère la dernière photo prise par le drone et l'affiche dans le fragment
     */
    private void display(){
        controlQuery.setValue(new Pair<>(controlQuery.getValue().first, true));
        droneService.getVideo(
                interventionId,
                response -> {
                    decodedImage = Base64.decode(response, Base64.DEFAULT);
                    decodedByte = BitmapFactory.decodeByteArray(decodedImage, 0, decodedImage.length);
                    videoView.setImageBitmap(decodedByte);
                    controlQuery.setValue(new Pair<>(controlQuery.getValue().first, false));
                },
                error -> {
                    error.printStackTrace();
                    controlQuery.setValue(new Pair<>(controlQuery.getValue().first, false));
                }
        );
    }

    /**
     * Affiche ou non la vidéo
     * @param shouldDisplay true si on veut chercher la dernière photo prise par le drone, false si on veut arrêter l'update des photos
     */
    public void setShouldDisplay(boolean shouldDisplay) {
        controlQuery.setValue(new Pair<>(shouldDisplay, controlQuery.getValue().second));
    }
}

package fr.istic.pit.frontend.ui.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import fr.istic.pit.frontend.R;
import fr.istic.pit.frontend.POIUtils;
import fr.istic.pit.frontend.model.Resource;

/**
 * Adapter lié à un RecyclerView pour afficher la liste des Resources non placées sur la carte
 */
public class ResourceCRMAdapter extends RecyclerView.Adapter<ResourceCRMAdapter.ResourceViewHolder> {

    private List<Resource> resources = new ArrayList<>();
    private OnItemClickListener mListener;
    private Context context;
    private int selectedItem = RecyclerView.NO_POSITION;

    public interface OnItemClickListener{
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener mListener){
        this.mListener=mListener;
    }

    public ResourceCRMAdapter(Context context){
        this.context=context;
    }

    public static class ResourceViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        ImageView icon;
        public ConstraintLayout layout;

        ResourceViewHolder(View v, final OnItemClickListener listener) {
            super(v);
            name = v.findViewById(R.id.resourceName);
            icon = v.findViewById(R.id.resourceImage);
            layout = v.findViewById(R.id.gridLayoutItem);
            v.setOnClickListener(v1 -> {
                if(listener != null){
                    int position = getAdapterPosition();
                    if(position!=RecyclerView.NO_POSITION){
                        listener.onItemClick(position);
                    }
                }
            });
        }
    }


    public void setResources(List<Resource> resources){
        this.resources = resources;
    }

    public void setSelectedItem(int position){
        int old_pos = selectedItem;
        //selectedItem = getInterventionAt(position);
        selectedItem = position;
        notifyItemChanged(old_pos);
        notifyItemChanged(selectedItem);
    }

    public Resource getSelectedItem(){
        return resources.get(selectedItem);
    }


    @NonNull
    @Override
    public ResourceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.resource_crm_item, parent, false);
        return new ResourceViewHolder(itemView, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ResourceViewHolder holder, int position) {
        Resource res = resources.get(position);
        holder.name.setText(res.name);
        String pictoType = POIUtils.getResourcePictoTypeFromRole(res.role, res.microState, res.macroState, res.crm);
        holder.icon.setImageDrawable(context.getDrawable(POIUtils.getDrawable(pictoType)));
        holder.itemView.setBackgroundColor(selectedItem == position ? Color.parseColor("#909090") : Color.TRANSPARENT);
    }

    @Override
    public int getItemCount() {
        return resources.size();
    }

}
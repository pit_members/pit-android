package fr.istic.pit.frontend;

import java.util.ArrayList;
import java.util.List;

import fr.istic.pit.frontend.model.PointOfInterest;

/**
 * Classe utilitaire pour les {@link PointOfInterest}
 */
public class POIUtils {

    /**
     * Constantes : les valeurs possibles pour le microstate et macrostate d'une ressource
     */
    public  static final String WAITING = "WAITING";
    public  static final String COMING = "COMING";
    public  static final String ARRIVED = "ARRIVED";
    public  static final String END = "END";
    public  static final String MOVING = "MOVING";
    public  static final String IDLE = "IDLE";

    /**
     * Constantes : les valeurs associées aux rôles des ressources
     */
    public static final int EAU = 4;
    public static final int FEU = 1;
    public static final int HUMAIN = 3;
    public static final int RISQUE = 2;

    /**
     * Constantes : les codes icônes des poi
     */
    public static final String DEFAULT = "default";
    public static final String POINT_RAVITAILLEMENT = "P_R";
    public static final String POINT_SENSIBLE_EAU = "PS_E";
    public static final String POINT_SENSIBLE_FEU = "PS_F";
    public static final String POINT_SENSIBLE_HUMAIN = "PS_H";
    public static final String POINT_SENSIBLE_RISQUE = "PS_R";
    public static final String PRISE_EAU_NON_PERENNE = "PE_NP";
    public static final String PRISE_EAU_PERENNE = "PE_P";
    public static final String SECTEUR_CHEF_COLONNE_EAU = "SCC_E";
    public static final String SECTEUR_CHEF_COLONNE_FEU = "SCC_F";
    public static final String SECTEUR_CHEF_COLONNE_HUMAIN = "SCC_H";
    public static final String SECTEUR_CHEF_COLONNE_RISQUE = "SCC_R";
    public static final String SECTEUR_CHEF_GROUPE_EAU = "SCG_E";
    public static final String SECTEUR_CHEF_GROUPE_FEU = "SCG_F";
    public static final String SECTEUR_CHEF_GROUPE_HUMAIN = "SCG_H";
    public static final String SECTEUR_CHEF_GROUPE_RISQUE = "SCG_R";
    public static final String SECTEUR_CHEF_SITE_EAU = "SCS_E";
    public static final String SECTEUR_CHEF_SITE_FEU = "SCS_F";
    public static final String SECTEUR_CHEF_SITE_HUMAIN = "SCS_H";
    public static final String SECTEUR_CHEF_SITE_RISQUE = "SCS_R";
    public static final String SOURCE_DANGER_EAU = "SD_E";
    public static final String SOURCE_DANGER_FEU = "SD_F";
    public static final String SOURCE_DANGER_HUMAIN = "SD_H";
    public static final String SOURCE_DANGER_RISQUE = "SD_R";
    public static final String CA_ETOILE_EAU = "CA_E_E";
    public static final String CA_ETOILE_FEU = "CA_E_F";
    public static final String CA_ETOILE_HUMAIN = "CA_E_H";
    public static final String CA_ETOILE_RISQUE = "CA_E_R";
    public static final String ZA_POLY_EAU = "ZA_P_E";
    public static final String ZA_POLY_FEU = "ZA_P_F";
    public static final String ZA_POLY_HUMAIN = "ZA_P_H";
    public static final String ZA_POLY_RISQUE = "ZA_P_R";
    public static final String CA_RECT_EAU = "CA_R_E";
    public static final String CA_RECT_FEU = "CA_R_F";
    public static final String CA_RECT_HUMAIN = "CA_R_H";
    public static final String CA_RECT_RISQUE = "CA_R_R";

    /**
     * Constantes : les codes icônes des ressources
     */
    public static final String VEHICULE_EAU_ACTIF = "V_E_A";
    public static final String VEHICULE_FEU_ACTIF = "V_F_A";
    public static final String VEHICULE_HUMAIN_ACTIF = "V_H_A";
    public static final String VEHICULE_RISQUE_ACTIF = "V_R_A";
    public static final String VEHICULE_EAU_PREVU = "V_E_P";
    public static final String VEHICULE_FEU_PREVU = "V_F_P";
    public static final String VEHICULE_HUMAIN_PREVU = "V_H_P";
    public static final String VEHICULE_RISQUE_PREVU = "V_R_P";

    private POIUtils() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Récupère le drawable associé à une icône
     * @param id le code icône
     * @return l'id du drawable associé à l'icône
     */
    public static int getDrawable(String id){
        int drawable;
        switch (id){
            case POINT_RAVITAILLEMENT: drawable = R.drawable.ic_point_ravitaillement; break;
            case POINT_SENSIBLE_EAU: drawable = R.drawable.ic_point_sensible_eau; break;
            case POINT_SENSIBLE_FEU: drawable = R.drawable.ic_point_sensible_feu; break;
            case POINT_SENSIBLE_HUMAIN: drawable = R.drawable.ic_point_sensible_humain; break;
            case POINT_SENSIBLE_RISQUE: drawable = R.drawable.ic_point_sensible_risque; break;
            case PRISE_EAU_NON_PERENNE: drawable = R.drawable.ic_prise_eau_non_perenne; break;
            case PRISE_EAU_PERENNE: drawable = R.drawable.ic_prise_eau_perenne; break;
            case SECTEUR_CHEF_COLONNE_EAU: drawable = R.drawable.ic_secteur_chef_colonne_eau; break;
            case SECTEUR_CHEF_COLONNE_FEU: drawable = R.drawable.ic_secteur_chef_colonne_feu; break;
            case SECTEUR_CHEF_COLONNE_HUMAIN: drawable = R.drawable.ic_secteur_chef_colonne_humain; break;
            case SECTEUR_CHEF_COLONNE_RISQUE: drawable = R.drawable.ic_secteur_chef_colonne_risque; break;
            case SECTEUR_CHEF_GROUPE_EAU: drawable = R.drawable.ic_secteur_chef_groupe_eau; break;
            case SECTEUR_CHEF_GROUPE_FEU: drawable = R.drawable.ic_secteur_chef_groupe_feu; break;
            case SECTEUR_CHEF_GROUPE_HUMAIN: drawable = R.drawable.ic_secteur_chef_groupe_humain; break;
            case SECTEUR_CHEF_GROUPE_RISQUE: drawable = R.drawable.ic_secteur_chef_groupe_risque; break;
            case SECTEUR_CHEF_SITE_EAU: drawable = R.drawable.ic_secteur_chef_site_eau; break;
            case SECTEUR_CHEF_SITE_FEU: drawable = R.drawable.ic_secteur_chef_site_feu; break;
            case SECTEUR_CHEF_SITE_HUMAIN: drawable = R.drawable.ic_secteur_chef_site_humain; break;
            case SECTEUR_CHEF_SITE_RISQUE: drawable = R.drawable.ic_secteur_chef_site_risque; break;
            case SOURCE_DANGER_EAU: drawable = R.drawable.ic_source_danger_eau; break;
            case SOURCE_DANGER_FEU: drawable = R.drawable.ic_source_danger_feu; break;
            case SOURCE_DANGER_HUMAIN: drawable = R.drawable.ic_source_danger_humain; break;
            case SOURCE_DANGER_RISQUE: drawable = R.drawable.ic_source_danger_risque; break;
            case CA_ETOILE_EAU: drawable = R.drawable.ic_ca_etoile_eau; break;
            case CA_ETOILE_FEU: drawable = R.drawable.ic_ca_etoile_feu; break;
            case CA_ETOILE_HUMAIN: drawable = R.drawable.ic_ca_etoile_humain; break;
            case CA_ETOILE_RISQUE: drawable = R.drawable.ic_ca_etoile_risque; break;
            case ZA_POLY_EAU: drawable = R.drawable.ic_za_poly_eau; break;
            case ZA_POLY_FEU: drawable = R.drawable.ic_za_poly_feu; break;
            case ZA_POLY_HUMAIN: drawable = R.drawable.ic_za_poly_humain; break;
            case ZA_POLY_RISQUE: drawable = R.drawable.ic_za_poly_risque; break;
            case CA_RECT_EAU: drawable = R.drawable.ic_ca_rect_eau; break;
            case CA_RECT_FEU: drawable = R.drawable.ic_ca_rect_feu; break;
            case CA_RECT_HUMAIN: drawable = R.drawable.ic_ca_rect_humain; break;
            case CA_RECT_RISQUE: drawable = R.drawable.ic_ca_rect_risque; break;
            case VEHICULE_EAU_ACTIF: drawable = R.drawable.ic_vehicule_actif_eau; break;
            case VEHICULE_FEU_ACTIF: drawable = R.drawable.ic_vehicule_actif_feu; break;
            case VEHICULE_HUMAIN_ACTIF: drawable = R.drawable.ic_vehicule_actif_humain; break;
            case VEHICULE_RISQUE_ACTIF: drawable = R.drawable.ic_vehicule_actif_risque; break;
            case VEHICULE_EAU_PREVU: drawable = R.drawable.ic_vehicule_prevu_eau; break;
            case VEHICULE_FEU_PREVU: drawable = R.drawable.ic_vehicule_prevu_feu; break;
            case VEHICULE_HUMAIN_PREVU: drawable = R.drawable.ic_vehicule_prevu_humain; break;
            case VEHICULE_RISQUE_PREVU: drawable = R.drawable.ic_vehicule_prevu_risque; break;
            default: drawable = R.drawable.marker_default; break;
        }
        return drawable;
    }

    /**
     * Renvoie la description associée à un poi
     * @param id le code icône du poi
     * @return la description associée à ce poi
     */
    public static String getDescription(String id){
        String description;
        switch (id){
            case POINT_RAVITAILLEMENT: description="point de ravitaillement"; break;
            case POINT_SENSIBLE_EAU: description="point sensible eau"; break;
            case POINT_SENSIBLE_FEU: description="point sensible feu"; break;
            case POINT_SENSIBLE_HUMAIN: description="point sensible humain"; break;
            case POINT_SENSIBLE_RISQUE: description="point sensible risque"; break;
            case PRISE_EAU_NON_PERENNE: description="prise eau non perenne"; break;
            case PRISE_EAU_PERENNE: description="prise eau perenne"; break;
            case SECTEUR_CHEF_COLONNE_EAU: description="secteur chef colonne eau"; break;
            case SECTEUR_CHEF_COLONNE_FEU: description="secteur chef colonne feu"; break;
            case SECTEUR_CHEF_COLONNE_HUMAIN: description="secteur chef colonne personne"; break;
            case SECTEUR_CHEF_COLONNE_RISQUE: description="secteur chef colonne risque"; break;
            case SECTEUR_CHEF_GROUPE_EAU: description="secteur chef groupe eau"; break;
            case SECTEUR_CHEF_GROUPE_FEU: description="secteur chef groupe feu"; break;
            case SECTEUR_CHEF_GROUPE_HUMAIN: description="secteur chef groupe humain"; break;
            case SECTEUR_CHEF_GROUPE_RISQUE: description="secteur chef groupe risque"; break;
            case SECTEUR_CHEF_SITE_EAU: description="secteur chef site eau"; break;
            case SECTEUR_CHEF_SITE_FEU: description="secteur chef site feu"; break;
            case SECTEUR_CHEF_SITE_HUMAIN: description="secteur chef site personne"; break;
            case SECTEUR_CHEF_SITE_RISQUE: description="secteur chef site risque"; break;
            case SOURCE_DANGER_EAU: description="source danger eau"; break;
            case SOURCE_DANGER_FEU: description="source danger feu"; break;
            case SOURCE_DANGER_HUMAIN: description="source danger humain"; break;
            case SOURCE_DANGER_RISQUE: description="source danger risque"; break;
            case CA_ETOILE_EAU: description="ca etoile eau"; break;
            case CA_ETOILE_FEU: description="ca etoile feu"; break;
            case CA_ETOILE_HUMAIN: description="ca etoile humain"; break;
            case CA_ETOILE_RISQUE: description="ca etoile risque"; break;
            case ZA_POLY_EAU: description="za poly eau"; break;
            case ZA_POLY_FEU: description="za poly feu"; break;
            case ZA_POLY_HUMAIN: description="za poly humain"; break;
            case ZA_POLY_RISQUE: description="za poly risque"; break;
            case CA_RECT_EAU: description="ca rect eau"; break;
            case CA_RECT_FEU: description="ca rect feu"; break;
            case CA_RECT_HUMAIN: description="ca rect humain"; break;
            case CA_RECT_RISQUE: description="ca rect risque"; break;
            default: description= DEFAULT; break;
        }
        return description;
    }

    /**
     * Récupère la liste des icônes poi
     * @return une liste de codes icônes
     */
    public static List<String> getListOfUniqueIcons(){
        List<String> list = new ArrayList<>();
        list.add(POINT_RAVITAILLEMENT);
        list.add(POINT_SENSIBLE_EAU);
        list.add(POINT_SENSIBLE_FEU);
        list.add(POINT_SENSIBLE_HUMAIN);
        list.add(POINT_SENSIBLE_RISQUE);
        list.add(PRISE_EAU_NON_PERENNE);
        list.add(PRISE_EAU_PERENNE);
        list.add(SOURCE_DANGER_EAU);
        list.add(SOURCE_DANGER_FEU);
        list.add(SOURCE_DANGER_HUMAIN);
        list.add(SOURCE_DANGER_RISQUE);
        list.add(CA_ETOILE_EAU);
        list.add(CA_ETOILE_FEU);
        list.add(CA_ETOILE_HUMAIN);
        list.add(CA_ETOILE_RISQUE);
        list.add(ZA_POLY_EAU);
        list.add(ZA_POLY_FEU);
        list.add(ZA_POLY_HUMAIN);
        list.add(ZA_POLY_RISQUE);
        list.add(CA_RECT_EAU);
        list.add(CA_RECT_FEU);
        list.add(CA_RECT_HUMAIN);
        list.add(CA_RECT_RISQUE);
        list.add(SECTEUR_CHEF_COLONNE_EAU);
        list.add(SECTEUR_CHEF_COLONNE_FEU);
        list.add(SECTEUR_CHEF_COLONNE_HUMAIN);
        list.add(SECTEUR_CHEF_COLONNE_RISQUE);
        list.add(SECTEUR_CHEF_GROUPE_EAU);
        list.add(SECTEUR_CHEF_GROUPE_FEU);
        list.add(SECTEUR_CHEF_GROUPE_HUMAIN);
        list.add(SECTEUR_CHEF_GROUPE_RISQUE);
        list.add(SECTEUR_CHEF_SITE_EAU);
        list.add(SECTEUR_CHEF_SITE_FEU);
        list.add(SECTEUR_CHEF_SITE_HUMAIN);
        list.add(SECTEUR_CHEF_SITE_RISQUE);
        return list;
    }

    /**
     * Récupère le code icône à associer à un marker en fonction du role, du microstate, du macrostate
     * et de la présence ou non dans le crm de sa ressource
     * @param role le role de la ressource
     * @param microState le microstate de la ressource
     * @param macroState le macrostate de la ressource
     * @param crm booléen indiquant la présence ou non de la ressource dans le crm
     * @return le code icône correspondant aux critères
     */
    public static String getResourcePictoTypeFromRole (Integer role, String microState, String macroState, boolean crm){
        String pictoType;
        boolean dispoCRM = macroState.equals(ARRIVED) && crm;
        boolean idleOnMap = microState.equals(IDLE) && !crm;
        switch (role){
            case EAU:
                if ( idleOnMap || dispoCRM)
                    pictoType = VEHICULE_EAU_ACTIF;
                else
                    pictoType= VEHICULE_EAU_PREVU;
                break;
            case FEU:
                if ( idleOnMap || dispoCRM)
                    pictoType = VEHICULE_FEU_ACTIF;
                else
                    pictoType = VEHICULE_FEU_PREVU;
                break;
            case HUMAIN:
                if (idleOnMap || dispoCRM)
                    pictoType = VEHICULE_HUMAIN_ACTIF;
                else
                    pictoType = VEHICULE_HUMAIN_PREVU;
                break;
            case RISQUE:
                if (idleOnMap || dispoCRM)
                    pictoType = VEHICULE_RISQUE_ACTIF;
                else
                    pictoType = VEHICULE_RISQUE_PREVU;
                break;
            default:
                pictoType = VEHICULE_EAU_ACTIF;
                break;
        }
        return pictoType;
    }

}

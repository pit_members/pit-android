package fr.istic.pit.frontend.service;

import android.content.Context;

import androidx.annotation.Nullable;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public abstract class AbstractService {
    protected static String URL = "http://pitprojectm2.ddns.net:11001";
    protected RequestQueue requestQueue;

    protected static class EmptyResponseJsonObjectRequest extends JsonObjectRequest {
        private boolean isCodis;

        public EmptyResponseJsonObjectRequest(int method, String url, @Nullable JSONObject jsonRequest, Response.Listener<JSONObject> listener, @Nullable Response.ErrorListener errorListener) {
            this(method, url, jsonRequest, listener, errorListener, false);
        }

        public EmptyResponseJsonObjectRequest(int method, String url, @Nullable JSONObject jsonRequest, Response.Listener<JSONObject> listener, @Nullable Response.ErrorListener errorListener, boolean isCodis) {
            super(method, url, jsonRequest, listener, errorListener);
            this.isCodis = isCodis;
        }

        @Override
        protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
            if(response.data.length == 0) {
                byte[] responseData = "{}".getBytes(StandardCharsets.UTF_8);
                response = new NetworkResponse(response.statusCode, responseData, response.notModified, response.networkTimeMs, response.allHeaders);
            }
            return super.parseNetworkResponse(response);
        }

        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
            Map<String, String> headers = new HashMap<>(super.getHeaders());
            if(isCodis)
                headers.put("X-PIT-Role-Codis", "true");
            return headers;
        }
    }

    protected static class EmptyResponseJsonArrayRequest extends JsonArrayRequest {

        private boolean isCodis;

        public EmptyResponseJsonArrayRequest(int method, String url, @Nullable JSONArray jsonRequest, Response.Listener<JSONArray> listener, @Nullable Response.ErrorListener errorListener) {
            this(method, url, jsonRequest, listener, errorListener, false);
        }

        public EmptyResponseJsonArrayRequest(int method, String url, @Nullable JSONArray jsonRequest, Response.Listener<JSONArray> listener, @Nullable Response.ErrorListener errorListener, boolean isCodis) {
            super(method, url, jsonRequest, listener, errorListener);
            this.isCodis = isCodis;
        }

        @Override
        protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
            if(response.data.length == 0) {
                byte[] responseData = "[]".getBytes(StandardCharsets.UTF_8);
                response = new NetworkResponse(response.statusCode, responseData, response.notModified, response.networkTimeMs, response.allHeaders);
            }
            return super.parseNetworkResponse(response);
        }

        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
            Map<String, String> headers = new HashMap<>(super.getHeaders());
            if(isCodis)
                headers.put("X-PIT-Role-Codis", "true");
            return headers;
        }
    }

    protected AbstractService(Context context) {
        requestQueue = Volley.newRequestQueue(context);
        requestQueue.start();
    }
}

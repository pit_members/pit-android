package fr.istic.pit.frontend.service;

import android.content.Context;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginService extends AbstractService {

    public static LoginService getService(Context context) {
        return new LoginService(context);
    }

    private LoginService(Context context) {
        super(context);
    }

    public void login(String login, String password, Response.Listener<JSONObject> onSuccess, Response.ErrorListener onError) {
        JSONObject json = new JSONObject();
        try {
            json.put("login", login);
            json.put("password", password);
            JsonObjectRequest request = new EmptyResponseJsonObjectRequest(Request.Method.POST, URL + "/login", json, onSuccess, onError);
            requestQueue.add(request);
        } catch (JSONException e) {
            // TODO : Log the error and response (Toast?)
        }
    }
}

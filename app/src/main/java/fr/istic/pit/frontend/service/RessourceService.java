package fr.istic.pit.frontend.service;

import android.content.Context;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import fr.istic.pit.frontend.model.Mapper;
import fr.istic.pit.frontend.model.Resource;

/**
 * Service effectuant les requêtes au back pour les {@link Resource}
 */
public class RessourceService extends AbstractService {

    public static RessourceService getService(Context context) {
        return new RessourceService(context);
    }

    protected RessourceService(Context context) {
        super(context);
    }

    /**
     * Récupère les ressources associées à une intervention
     * @param interventionId ID de l'intervention
     * @param onSuccess callback si succès
     * @param onError callback si erreur
     */
    public void getRessourceListByInterventionId(Long interventionId, Response.Listener<JSONArray> onSuccess, Response.ErrorListener onError) {

        JsonArrayRequest request = new EmptyResponseJsonArrayRequest(
                Request.Method.GET,
                URL + "/interventions/" + interventionId + "/resources",
                null,
                onSuccess,
                onError);
        requestQueue.add(request);
    }

    /**
     * Récupère les ressources associées à une intervention en fonction du macrostate et du
     * microstate
     * @param macro le macrostate
     * @param micro le microstate
     * @param onSuccess callback si succès
     * @param onError callback si erreur
     */
    public void getRessourceListByState(String macro, String micro, Response.Listener<JSONArray> onSuccess, Response.ErrorListener onError) {
        String requeteString = URL + "/resources";
        String macroParameter = !TextUtils.isEmpty(macro) && !TextUtils.isEmpty(macro.trim()) ? "macro=" + macro : "";
        String microParameter = !TextUtils.isEmpty(micro) && !TextUtils.isEmpty(micro.trim()) ? "micro=" + micro : "";
        if(!TextUtils.isEmpty(macroParameter) && !TextUtils.isEmpty(microParameter))
            requeteString += "?" + macroParameter + "&" + microParameter;
        else if(!TextUtils.isEmpty(macroParameter))
            requeteString += "?" + macroParameter;
        else if(!TextUtils.isEmpty(microParameter))
            requeteString += "?" + microParameter;

        JsonArrayRequest request = new EmptyResponseJsonArrayRequest(
                Request.Method.GET,
                requeteString,
                null,
                onSuccess,
                onError);
        requestQueue.add(request);
    }

    /**
     * Accepte une demande de moyen pour une intervention
     * @param interventionId ID de l'intervention
     * @param ressourceId ID de la ressource
     * @param isCodis booléen
     * @param name nom de la ressource
     * @param onSuccess callback si succès
     * @param onError callback si erreur
     */
    public void acceptRessource(Long interventionId, Long ressourceId, boolean isCodis, String name, Response.Listener<JSONObject> onSuccess, Response.ErrorListener onError) {
        JSONObject json = new JSONObject();
        try {
            json.put("name", name);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new EmptyResponseJsonObjectRequest(
                Request.Method.PATCH,
                URL + "/interventions/" + interventionId + "/resources/" + ressourceId + "/accept",
                json,
                onSuccess,
                onError,
                isCodis);
        requestQueue.add(request);
    }

    /**
     * Refuse ou annule une demande de moyens pour une intervention
     * @param interventionId ID de l'intervention
     * @param ressourceId ID de la ressource
     * @param isCodis booléen
     * @param onSuccess callback si succès
     * @param onError callback si erreur
     */
    public void rejectRessource(Long interventionId, Long ressourceId, boolean isCodis, Response.Listener<JSONObject> onSuccess, Response.ErrorListener onError) {
        JsonObjectRequest request = new EmptyResponseJsonObjectRequest(
                Request.Method.PATCH,
                URL + "/interventions/" + interventionId + "/resources/" + ressourceId + "/reject",
                null,
                onSuccess,
                onError,
                isCodis);
        requestQueue.add(request);
    }

    /**
     * Valide l'arrivée sur les lieux d'une ressource
     * @param interventionId ID de l'intervention
     * @param ressourceId ID de la ressource
     * @param onSuccess callback si succès
     * @param onError callback si erreur
     */
    public void arriveRessource(Long interventionId, Long ressourceId, Response.Listener<JSONObject> onSuccess, Response.ErrorListener onError) {
        JsonObjectRequest request = new EmptyResponseJsonObjectRequest(
                Request.Method.PATCH,
                URL + "/interventions/" + interventionId + "/resources/" + ressourceId + "/arrive",
                null,
                onSuccess,
                onError);
        requestQueue.add(request);
    }

    /**
     * Déplace une ressource
     * @param interventionId ID de l'intervention
     * @param ressourceId ID de la ressource
     * @param latitude la latitude updatée
     * @param longitude la longitude updatée
     * @param onSuccess callback si succès
     * @param onError callback si erreur
     */
    public void moveRessource(Long interventionId, Long ressourceId, double latitude, double longitude, Response.Listener<JSONObject> onSuccess, Response.ErrorListener onError) {
        JSONObject coordinate = new JSONObject();
        try {
            coordinate.put("latitude", latitude);
            coordinate.put("longitude", longitude);
            JsonObjectRequest request = new EmptyResponseJsonObjectRequest(
                    Request.Method.PATCH,
                    URL + "/interventions/" + interventionId + "/resources/" + ressourceId + "/move",
                    coordinate,
                    onSuccess,
                    onError);
            requestQueue.add(request);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Met à jour le rôle d'une ressource
     * @param interventionId ID de l'intervention
     * @param ressourceId ID de la ressource
     * @param role rôle updaté
     * @param onSuccess callback si succès
     * @param onError callback si erreur
     */
    public void roleRessource(Long interventionId, Long ressourceId, Integer role, Response.Listener<JSONObject> onSuccess, Response.ErrorListener onError) {
        JSONObject ressourceRole = new JSONObject();
        try {
            ressourceRole.put("role", role);
            JsonObjectRequest request = new EmptyResponseJsonObjectRequest(
                    Request.Method.PATCH,
                    URL + "/interventions/" + interventionId + "/resources/" + ressourceId + "/role",
                    ressourceRole,
                    onSuccess,
                    onError);
            requestQueue.add(request);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Renvoie une ressource au crm
     * @param interventionId ID de l'intervention
     * @param ressourceId ID de la ressource
     * @param onSuccess callback si succès
     * @param onError callback si erreur
     */
    public void crmRessource(Long interventionId, Long ressourceId, Response.Listener<JSONObject> onSuccess, Response.ErrorListener onError) {
        JsonObjectRequest request = new EmptyResponseJsonObjectRequest(
                Request.Method.PATCH,
                URL + "/interventions/" + interventionId + "/resources/" + ressourceId + "/crm",
                null,
                onSuccess,
                onError);
        requestQueue.add(request);
    }

    /**
     * Libère un moyen d'une intervention
     * @param interventionId ID de l'intervention
     * @param ressourceId ID de la ressource
     * @param onSuccess callback si succès
     * @param onError callback si erreur
     */
    public void releaseRessource(Long interventionId, Long ressourceId, Response.Listener<JSONObject> onSuccess, Response.ErrorListener onError) {
        JsonObjectRequest request = new EmptyResponseJsonObjectRequest(
                Request.Method.PATCH,
                URL + "/interventions/" + interventionId + "/resources/" + ressourceId + "/release",
                null,
                onSuccess,
                onError);
        requestQueue.add(request);
    }

    /**
     * Demande des moyens pour une intervention
     * @param interventionId ID de l'intervention
     * @param resources les ressources demandées
     * @param onSuccess callback si succès
     * @param onError callback si erreur
     * @throws JSONException
     */
    public void requestResource(Long interventionId, List<Resource> resources, Response.Listener<JSONArray> onSuccess, Response.ErrorListener onError) throws JSONException {
            JsonArrayRequest request = new EmptyResponseJsonArrayRequest(
                    Request.Method.POST,
                    URL + "/interventions/" + interventionId + "/resources/bulk",
                    Mapper.resourceToJSON(resources),
                    onSuccess,
                    onError);
            requestQueue.add(request);
    }
}




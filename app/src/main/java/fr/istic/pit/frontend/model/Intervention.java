package fr.istic.pit.frontend.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Intervention implements Parcelable {
    public Long id;

    /**
     * Identifiant métier unique de l'intervention
     */
    public String name;

    /**
     * Code sinistre
     */
    public String sinisterCode;

    /**
     * Heure de début d'intervention
     */
    public Date startTime;

    /**
     * Heure de fin d'intervention
     */
    public Date endTime;

    /**
     * Latitude
     */
    public Double latitude;

    /**
     * Longitude
     */
    public Double longitude;

    /**
     * Informations complémentaires
     */
    public String informations;

    /**
     * Liste des points d'intérêts pour l'intervention
     */
    public List<PointOfInterest> pointsOfInterest = new ArrayList<>();

    /**
     * Liste des moyens pour l'intervention
     */
    public List<Resource> resources = new ArrayList<>();


    public String droneAvailability;

    public Intervention(){}

    /**
     *
     * @param in Parcel pour recréer intervention à partir d'un intent extra
     */
    protected Intervention(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readLong();
        }
        name = in.readString();
        sinisterCode = in.readString();
        if (in.readByte() == 0) {
            latitude = null;
        } else {
            latitude = in.readDouble();
        }
        if (in.readByte() == 0) {
            longitude = null;
        } else {
            longitude = in.readDouble();
        }
        informations = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(id);
        }
        dest.writeString(name);
        dest.writeString(sinisterCode);
        if (latitude == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(latitude);
        }
        if (longitude == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(longitude);
        }
        dest.writeString(informations);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Intervention> CREATOR = new Creator<Intervention>() {
        @Override
        public Intervention createFromParcel(Parcel in) {
            return new Intervention(in);
        }

        @Override
        public Intervention[] newArray(int size) {
            return new Intervention[size];
        }
    };
}

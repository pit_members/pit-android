package fr.istic.pit.frontend.ui.activities;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import org.osmdroid.config.Configuration;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.compass.CompassOverlay;
import org.osmdroid.views.overlay.compass.InternalCompassOrientationProvider;

import fr.istic.pit.frontend.R;

/**
 * Vue de selection d'un point sur la carte
 */
public class InterventionCreationMap extends AppCompatActivity {

    private MapView map;
    private String lat;
    private String lon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intervention_creation_map);
        map = findViewById(R.id.map);
        loadMap();

        // le bouton de validation renvoie à l'activité appelante les coordonnées actuelles
        Button validButton = findViewById(R.id.validButton);
        validButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lat = map.getMapCenter().getLatitude()+"";
                lon = map.getMapCenter().getLongitude()+"";
                Intent i = new Intent();
                i.putExtra("lat", lat.substring(0,9));
                i.putExtra("lon", lon.substring(0,9));
                InterventionCreationMap.this.setResult(RESULT_OK, i);
                finish();
            }
        });

    }

    /**
     * Permet de charger et afficher la carte
     */
    private void loadMap() {
        //permission de localisation et de lecture de mémoire pour récup la carte
        ActivityCompat.requestPermissions(InterventionCreationMap.this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        //chargement carte
        Configuration.getInstance().load(this.getApplicationContext(), PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext()));
        //positionnement sur la carte (zoom + emplacement) et affichage orientation via boussole
        map.getController().setZoom(15.0);
        map.setMultiTouchControls(true);
        map.getController().setCenter(new GeoPoint(48.112703,-1.659534));
        CompassOverlay compassOverlay = new CompassOverlay(this.getApplicationContext(), new InternalCompassOrientationProvider(this.getApplicationContext()), map);
        compassOverlay.enableCompass();
        map.getOverlays().add(compassOverlay);
    }

}

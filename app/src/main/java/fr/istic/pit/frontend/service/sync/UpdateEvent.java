package fr.istic.pit.frontend.service.sync;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Objet servant à dé-sérialiser un évènement de mise à jour reçue
 * depuis le broker de messages
 */
public class UpdateEvent {
    /**
     * Le type d'évènement de mise à jour
     */
    private final UpdateEventType eventType;

    /**
     * La date/heure d'émission de l'évènement
     */
    private final Date eventTime;

    /**
     * Le numéro d'évènement
     */
    private final long eventNo;

    /**
     * Les données supplémentaires attachées au message
     */
    private final JSONObject data;

    /**
     * Constructeur de base
     * @param json L'objet JSON contenant le message MQTT
     */
    UpdateEvent(JSONObject json) {
        eventType = parseType(json.optString("eventType"));
        eventTime = parseDate(json.optString("eventTime"));
        eventNo = json.optLong("eventNo");
        JSONObject data = json.optJSONObject("data");
        this.data = data != null ? data : new JSONObject();
    }

    /**
     * Accesseur pour le type
     * @return Le type d'évènement
     */
    public UpdateEventType getType() {
        return eventType;
    }

    /**
     * Accesseur pour la date
     * @return La date/heure d'émission
     */
    public Date getTime() {
        return eventTime;
    }

    /**
     * Accesseur pour le numéro
     * @return Le numéro de message
     */
    public long getNo() {
        return eventNo;
    }

    /**
     * Accesseur pour les données supplémentaires
     * @return Les données supplémentaires sous forme d'un objet JSON
     */
    public JSONObject getData() {
        return data;
    }

    /**
     * Récupère la date/heure à partir d'une chaîne de caractères stockant la date et l'heure
     * au format standard JSON (ISO 8601)
     * @param date La chaîne représentant la date
     * @return Un objet Date correspondant à la date récupérée, ou null si la date n'a pas pu être
     * analysée
     */
    private Date parseDate(String date) {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
        try {
            return format.parse(date);
        } catch (ParseException e) {
            return null;
        }
    }

    /**
     * Récupère l'énumération correspondante au type d'évènement à partir du nom de la constante
     * contenue dans l'objet JSON
     * @param name Le nom de la constante
     * @return La constante d'énumération, ou null si aucune ne correspond
     */
    private UpdateEventType parseType(String name) {
        for (UpdateEventType type : UpdateEventType.values()) {
            if (type.name().equals(name)) {
                return type;
            }
        }
        return null;
    }

    /**
     * Représentation textuelle de l'objet
     * @return Une chaîne de caractères décrivant l'objet
     */
    @Override
    public String toString() {
        return "UpdateEvent{" +
                "eventType=" + eventType +
                ", eventTime=" + eventTime +
                ", eventNo=" + eventNo +
                '}';
    }
}
